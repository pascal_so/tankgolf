#include <catch2/catch.hpp>

#include <types/protocol_object_types.hpp>

TEST_CASE("ProtocolObject") {
    SECTION("isQuery") {
        REQUIRE(isQuery(QueryShoot{}));
        REQUIRE(isQuery(QueryRespawn{}));
        REQUIRE(isQuery(FullQueryShoot{}));
        REQUIRE(isQuery(FullQueryRespawn{}));

        REQUIRE(!isQuery(RespawnRoundStart{}));
        REQUIRE(!isQuery(ShootRoundStart{}));
        REQUIRE(!isQuery(RespawnCommit{}));
        REQUIRE(!isQuery(ShootCommit{}));
    }

    SECTION("string serializer") {
        ProtocolObject queryShoot = QueryShoot{0.5, 1.0};
        ProtocolObject queryRespawn = QueryRespawn{4.0};

        auto strQueryShoot = formatStringProtocol(queryShoot);
        auto strQueryRespawn = formatStringProtocol(queryRespawn);

        REQUIRE(strQueryShoot == "queryshoot 0.5 1");
        REQUIRE(strQueryRespawn == "queryrespawn 4");
    }
}
