#include <catch2/catch.hpp>
#include <types/history_object_types.hpp>

TEST_CASE("Flip objects") {
    History::Event ev = History::FallOffMapEvent {1};
    History::HistoryObject obj (ev, 0, false);
    REQUIRE(boost::get<History::FallOffMapEvent>(obj.event).player_id == 1);
    
    SECTION("flip HistoryObject with FallOffMapEvent") {
        obj.flip();
        REQUIRE(obj.active_player_id == 1);
        REQUIRE(boost::get<History::FallOffMapEvent>(obj.event).player_id == 0);
        REQUIRE(obj.simulated == false);
    }

    SECTION("flip HistoryObject with Motion") {
        std::vector<PhysicsState> motion {PhysicsState ({Orientation({1.0, 2.0}, 0.5), Orientation({3.0, 4.0}, 1.5)},
                                                        point{5.0, 6.0})};
        obj = History::HistoryObject (motion, 0, false);
        obj.flip();
        REQUIRE(boost::get<History::Motion>(obj.event)[0].tanks == std::array<Orientation, 2>{
            Orientation({3.0, 4.0}, 1.5), Orientation({1.0, 2.0}, 0.5)
        });
        REQUIRE(boost::get<History::Motion>(obj.event)[0].bullet == point {5.0, 6.0});
        REQUIRE(obj.active_player_id == 1);
        REQUIRE(obj.simulated == false);
    }
}

TEST_CASE("get Score") {
    using namespace History;

    std::vector<HistoryObject> hist;

    SECTION("getScore works") {
        hist.emplace_back(FallOffMapEvent {1}, 0, false);
        REQUIRE(getScores(hist) == std::array<unsigned, 2>{1, 0});
    }

    SECTION("simulated events get ignored") {
        hist.emplace_back(FallOffMapEvent {1}, 0, true);
        REQUIRE(getScores(hist) == std::array<unsigned, 2>{0, 0});
    }

    SECTION("independent of active_player_id") {
        hist.emplace_back(FallOffMapEvent {1}, 1, false);
        REQUIRE(getScores(hist) == std::array<unsigned, 2>{1, 0});
    }

    SECTION("other events get ignored") {
        hist.emplace_back(RespawnEvent {5.f}, 0, false);
        hist.emplace_back(BulletImpactEvent {point{0, 0}}, 0, false);
        hist.emplace_back(BulletImpactEvent {point{0, 0}}, 0, true);
        REQUIRE(getScores(hist) == std::array<unsigned, 2>{0, 0});
    }

    SECTION("combined example") {
        hist.emplace_back(FallOffMapEvent {1}, 0, true);
        hist.emplace_back(FallOffMapEvent {1}, 0, false);
        hist.emplace_back(RespawnEvent {5.f}, 0, false);
        hist.emplace_back(FallOffMapEvent {0}, 0, false);
        hist.emplace_back(FallOffMapEvent {0}, 1, false);
        hist.emplace_back(BulletImpactEvent {point{0, 0}}, 0, false);

        REQUIRE(getScores(hist) == std::array<unsigned, 2>{1, 2});
    }
}