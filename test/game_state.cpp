#include <catch2/catch.hpp>
#include <types/game_state_types.hpp>
#include <sstream>

TEST_CASE("PhysicsState") {
    PhysicsState state ({Orientation({1.0, 2.0}, 0.5), Orientation({3.0, 4.0}, 1.5)},
                        point{5.0, 6.0});

    SECTION("string serializer") {
        std::stringstream ss;
        ss << state;

        REQUIRE(ss.str() == "1 2 0.5 3 4 1.5 5 6");
        
        SECTION("parser inverts serializer") {
            REQUIRE(parseStringPhysicsState(ss) == state);
        }
    }

    SECTION("json parser inverts serializer") {
        auto j = jsonPhysicsState(state);

        REQUIRE(parseJsonPhysicsState(j) == state);
    }

    SECTION("flip PhysicsState") {
        state.flip();

        REQUIRE(state.tanks == std::array<Orientation, 2>{
                Orientation({3.0, 4.0}, 1.5),
                Orientation({1.0, 2.0}, 0.5)});
        REQUIRE(state.bullet == point{5.0, 6.0});
    }
}


TEST_CASE("TankOrientations") {
    TankOrientations tank_orientations({Orientation({1.0, 2.0}, 0.5), Orientation({3.0, 4.0}, 1.5)});

    SECTION("string serializer") {
        std::stringstream ss;
        ss << tank_orientations;

        REQUIRE(ss.str() == "1 2 0.5 3 4 1.5");

        SECTION("parser inverts serializer") {
            REQUIRE(parseStringTankOrientations(ss) == tank_orientations);
        }
    }

    SECTION("json parser inverts serializer") {
        auto j = jsonTankOrientations(tank_orientations);

        REQUIRE(parseJsonTankOrientations(j) == tank_orientations);
    }
}
