#!/bin/bash

set -e

rm -rf build
mkdir build
cd build
cmake .. # -DTANKGOLF_VERBOSE=1
cmake --build . # -- VERBOSE=1
