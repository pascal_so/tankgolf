#!/bin/bash

# Removes anything that is not needed when distributing the
# framework. IMPORTANT: run `npm run build` first. This
# script does not delete the `.git` directory in order to
# reduce the attack surface for bad fuckups.

set -e

rm -rf .clang-format
rm -rf build
rm -rf .gitignore .gitmodules

rm -rf libs/Catch2
rm -rf libs/Box2D/Contributions
rm -rf libs/Box2D/Documentation
rm -rf libs/Box2D/.git
rm -rf libs/Box2D/HelloWorld
rm -rf libs/Box2D/Testbed
rm -rf libs/json/benchmark
rm -rf libs/json/doc
rm -rf libs/json/.git
rm -rf libs/json/test
rm -rf libs/Simple-Web-Server/.git
rm -rf libs/websocketpp/docs
rm -rf libs/websocketpp/.git
rm -rf libs/websocketpp/test
rm -rf libs/websocketpp/tutorials

rm -rf test

rm -rf *~
rm -rf src/*~
rm -rf src/util/*~
rm -rf src/types/*~
rm -rf src/fileserver/*~

rm -rf src/webclient/build
rm -rf src/webclient/config
rm -rf src/webclient/index.html
rm -rf src/webclient/node_modules
rm -rf src/webclient/package.json
rm -rf src/webclient/package-lock.json
rm -rf src/webclient/README.md
rm -rf src/webclient/static

rm -rf bots/spam_bot.cpp

rm -rf ignore/
rm -rf .circleci/

echo "Remeber to remove the tests from CMakeLists.txt (and maybe tankgolf_cli) and to remove the .git directory."
echo "Also make sure to change the version number in Readme.md."
echo "Check again for any ~ files, maybe run this script again"
echo "then finally remove this script"
