FROM ubuntu:18.04

EXPOSE 8000
EXPOSE 9002

RUN apt-get -qq update && apt-get -qq install git cmake g++ clang libboost-dev libboost-system-dev libboost-filesystem-dev vim

RUN mkdir tankgolf
WORKDIR tankgolf

COPY . .

RUN ./build.sh

WORKDIR build

CMD ./tankgolf
