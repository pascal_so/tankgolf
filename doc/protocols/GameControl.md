# Game Control Protocol

The participants interacts with the server through a browser. The browser
requests a new game from the server via a dedicated Websocket connection
per game. This connection is also responsible for stopping the game and
for receiving scores and visualization from the game server.

## Participants
* browser
* server: the game server, active at the main game server url and port.

## Protocol
Websocket connection is opened by browser. browser requests the Websocket
subprotocol "control".

The browser then immediately sends a single message of irrelevant content.

The server replies with the assigned game id.

From now on, all the messages exchanged are in the following format:
{
    'type': // specified below
    .. additional arguments
}

The different types are used as follows:

## Browser to Server

### Player Descriptions

The browser sends a json message of the following format:

{
    'type': 'player_descriptions',
    'players': [
        {
            'type': // see below
            // .. additional fields see below
        }
    ]
}

A player type is one of:
* 'websocket', which has no additional fields
* 'bot', followed by the field 'command' to start the bot.
* 'server' followed by the field 'url' containing the url to the server
    bot. Should probably be of the format 'ws://something:xxxx'

### Stop Game

format:
{
    'type': 'stop_game'
}

This ends the game and this Websocket connection can now be closed.

### Stop Server

format:
{
    'type': 'stop_server'
}

Stops all currently running games and terminates the server.

### Finished Playback

format:
{
    'type': 'finished_playback'
}

At the end of every round, after the 'End Round' message, the browser must
reply with 'finished_playback'. This is to prevent the browser from getting
flooded with events in the case of a bot vs bot game. The game server will not
start the next round until it receives this message.
Note however that the browser might still struggle if a bot runs hundreds of
queries and the results of all of those are sent to the browser, as the
calculations only wait for the playback to catch up at the end of a turn, not
between queries.

## Server to Browser

### Game History

format:
{
    'type': 'history',
    'history': [
        // one or multiple HistoryObject
    ]
}

As soon as the game is running (after the browser has sent the player
descriptions), the server keeps sending logs of game events and simulations
that can then be displayed in the browser. See History.md for a description
of the history format.

### Start Round

format:
{
    'type': 'start_round',
    'round_type': // 'respawn' or 'shoot'
    'player_id': // the id of the active player, zero-based
    'state': // the GameState in json format
}

### End Round

format:
{
    'type': 'end_round'
}

Notify the browser that the current round is over. (see above in 'Playback
Finished' for more details).
