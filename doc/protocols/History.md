# History Format

History is generally passed around as an array of History Objects, which in turn are either
a motion (vector of physics states) or a history event

## History Events

RespawnEvent
ShootEvent
FallOffMapEvent
BulletImpactEvent

## JSON Format

JSON representation of HistoryObject contains a 'type' field, which is one of:
"respawn"
"shoot"
"fall_off_map"
"impact"
"motion"
