# Player Communication Protocol

This protocol is used as soon as the connection to the player is set up and the
game is running. The protocol does not care about what kind of player is
communicating via it. There is a string and a json version of the protocol,
but the transmitted data is the same. Just make sure that both endpoints agree
on the version.

## Participants
* player: either a bot running locally as a subprocess of the game server,
  a manual player via Websocket connection, or a hidden source bot on a
  remote server.
* server: the game server

## String Protocol

TODO (see protocol_object_types.cpp)

## JSON Protocol

Every json object holds a type field, the list of other available fields is
dependent on this type.

TODO