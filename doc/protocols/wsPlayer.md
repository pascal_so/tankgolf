# Websocket Player Protocol

One or more of the players in a game can be controlled manually, using for
example WSAD or arrow keys controls. These controls are sent to the game
via a dedicated Websocket connection per player.

The connection is only opened once the game control connection is established
and the browser was told the assigned game id (see game control protocol for
more details).

## Participants
* player: the manual player in the browser
* server: the game server, active at the main game server url and port.

## Protocol
Websocket connection is opened by player. player requests the Websocket
subprotocol "player".

player sends a single line with two ints: game_id and player_id, separated by
a space.

From here on the communication follows the same player interaction protocol as
local bots or server bots. See player protocol. The json version of the
protocol is used.
