# Map Description

The map is loaded from a json file in `geometry/maps/`. The backend currently loads the map from a hardcoded path in `server.cpp`.
The frontend currently includes the map at compile time, see `PlayGame.vue`.

The map has to be made up of strictly convex polygons due to limitations of Box2D.

A map is made up of multiple platforms, that each have a shape and a material.

## Materials

A material is described by its integer index. 0 is a non slippery material, 1 is slippery. This list might be extended in the future.

## Shape description

The JSON object 'shape' is an array of convex polygons. A polygon is a list of {x:, y:} objects in ccw order.
