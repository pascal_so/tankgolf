# Game Hosts

## Server Game Host

A server game host allows a game to be played where one or both of the players
is controlled manually with the arrow keys/wsad. The host spawned on request by
the browser.

A main WebSocket connection, called the control connection, is active from the
browser to the game. In addition to that, there is a WebSocket connection for
every player that is controlled manually. See the protocol documentations for
`GameControl` and `wsPlayer` in the `protocols` directory.

The history is logged after every action, i.e. after a shoot/respawn simulation
or commit.

## Headless Game Host

Used for running bot vs bot games from the command line.
