# Terminology

I tried to stick to the following terminology as much as possible, feel free to
fix mistakes if you spot them.

* A **player** is either a bot or manual player. SOI participants coding and
testing their bots are only referred to as participants, not players. In user
facing places, the players are often called player A and B, instead their
indices, as to avoid confusion through zero based vs one based indexing.
* A **round**, also called **turn**, is either a shoot or respawn round, and
starts when a player receives the `shoot` or `respawn` command. The round
includes queries made by the player, and continues until the player commits to
a move (i.e. returns a shoot angle/intensity or respawn position).
* An **action** is either a shoot or respawn action, and can be a query or a
commit. A round is made up of zero or more query actions and one commit action.
