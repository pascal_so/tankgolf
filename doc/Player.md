# Player

The Player class handles an entire round (respawn/shoot), including queries and
the committed move. It receives state by the Game Host such that the player
represented by this instance of the Player class is the first player, i.e. the
state will be flipped by the Game Host for player B.

To handle the turn, the Player class has access to a physics engine instance,
which is owned by the Game Host.
