# Tank Golf v1.8

[![CircleCI](https://circleci.com/bb/pascal_so/tankgolf.svg?style=svg)](https://circleci.com/bb/pascal_so/tankgolf)

The easiest way to start using the Game Server is to download the latest version from the downloads page (https://bitbucket.org/pascal_so/tankgolf/downloads/tankgolf.zip) and to run `./build.sh`. Note that you'll have to install Boost first. You'll need at least version 1.65 of Boost, which you can either install from [here](https://www.boost.org/) or download from the repos of your linux distro:

```bash
sudo apt-get install libboost-dev libboost-system-dev libboost-filesystem-dev
```

Maybe you also need to install cmake. After building, there should be a program called `tankgolf` in the build directory. Run it and go to `localhost:8000` in your browser.

## Writing Bots

Name your bot anything that ends in `_bot.cpp`, so for example `my_first_bot.cpp`, and place it in the `bots` directory. Take a look at the existing bots in that directory to help you get started.

## Building on Windows

The easiest way to build TankGolf on Windows seems to be with MinGW. Try to use this MinGW distribution if you don't have any yet: https://nuwen.net/mingw.html
Make sure you install MinGW in `C:\MinGW\`

After that, download and open the CMake gui (https://cmake.org/download/). Enter the path to the tankgolf download and in the build path enter the tankgolf download path plus `\build` (see screenshot). Add the following variables:
```
BOOST_ROOT = C:\MinGW
BOOST_LIBRARYDIR = C:\MinGW\include
```

![CMake MinGW build](cmake-mingw.png)

Click "Configure" and choose "MinGW Makefiles" in the dropdown as the generator.
As soon as the configuring is done, click "Generate".

Now open your cmd, cd to the build directory and type:
```
C:\MinGW\bin\make
```

After this is done, you can start the Server with this command:
```
tankgolf.exe
```

Now go to `localhost:8000` in your browser.

## Rules

* The map during the contest will be the same as the one you see while developing your bots.
* The time to show the visualization does not count towards your allowed time to compute the result.
* We'll decide on the amount of points you need to win later. Your strategy should always be to make as many points as possible.

## Building manually on linux

Downloading the zipped version from the downloads page is the preferred option. See above.
Otherwise, clone this repository with the `--recursive` option, then run those commands (requires Boost, cmake and npm to be installed):

```bash
# build the backend
mkdir build
cd build
cmake ..
cmake --build .
cd ..

# build the frontend
cd src/webclient/
npm install
npm run build
```

Note that you don't have to worry about the frontend if you download the zipped version from the downloads page. (see above)

**Warning** the tankgolf backend binary currently has to be run from the `build` directory because the frontend files are found via relative path. Will be fixed eventually.

Alternatively, if you don't want to install boost on your machine, you can also build the provided `Dockerfile` with this command:
```bash
cd path/to/tankgolf
sudo docker image build -t tankgolf .
```
You will however have to build the frontend yourself first.

## Executing

Run the backend
```bash
cd build
./tankgolf
```

Then navigate to `localhost:8000` to see the frontend and if necessary check console output of the backend.

## Dev

To run the frontend with hot reloading:
```bash
cd src/webclient
npm install
npm run dev
```

The dev server will be running at `localhost:8080`.

## Todo

(roughly ordered by descending priority)

Make ServerGameHost inherit from GameHost

polish graphics, maybe add a cool blueprint effect when playing back queries

continue documentation

add more tests, especially integration tests

I guess we need more try catch blocks??

Make the server throw less errors on exit (how 2 websocketpp, anyone with experience on this?)

## Documenation

Check out the `doc` directory.

## Distribution

First build the frontend (see section `building` for this), then run `./strip-dist.sh`. This script removes all files and directories that are not needed for distribution (doesn't remove `.git` directory for safety reasons in case you mess up, and you still need to remove the `tester` and `tankgolf_cli` manually from `CMakeLists.txt`).

You can then zip the directory and upload it somewhere for easy installing.

After bigger changes, you can try to compile tankgolf with older versions of gcc to see if that works (the project initially didn't compile with gcc < 6, which is apparenlty still current on Mint..). Dockerfiles (although not optimized for build time in the slightest) are provided. Use the `-f` flag when building the docker image.
