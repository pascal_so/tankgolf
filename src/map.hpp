#ifndef MAP_HPP
#define MAP_HPP

#include <vector>
#include <array>
#include "box2d_polyfill.hpp"
#include <Box2D/Box2D.h>

#include "types/point.hpp"

struct Platform {
    std::vector<std::vector<point>> shape;
    int material;
};

class Map {
public:
    std::vector<Platform> platforms;
    float width, height;
    float y_limit;
    std::array<float, 2> respawn_x_limits;
    float respawn_height;
    point gravity;

    Map(std::string const& path);
    Map(Map const& m);

    bool inside(point const& p, const bool add_padding = true) const;

    b2Body* makeBox2DGround(b2World& world) const;
};

#endif // MAP_HPP
