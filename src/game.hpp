#ifndef GAME_HPP
#define GAME_HPP

#include <array>
#include <atomic>
#include <random>
#include <boost/variant.hpp>
#include <boost/optional.hpp>
#define _USE_MATH_DEFINES
#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979
#endif

#include "map.hpp"
#include "channel.hpp"
#include "physics.hpp"
#include "logger.hpp"
#include "types/control_object_types.hpp"
#include "types/game_state_types.hpp"
#include "types/protocol_object_types.hpp"
#include "types/history_object_types.hpp"
#include "types/point.hpp"
#include "util/blocking_queue.hpp"

class Player {
    std::unique_ptr<Channel<ProtocolObject>> channel;

    PhysicsEngine* physics_engine;

    std::vector<History::HistoryObject> handleQuery(ProtocolObject const& po,
                                                    TankOrientations const& tank_orientations);

    std::mt19937 rng;

public:
    PlayerType type;

    Player(std::unique_ptr<Channel<ProtocolObject>> channel, PhysicsEngine* engine, PlayerType const& type);

    std::pair<std::vector<History::HistoryObject>, GameState>
    turn(const GameState& state, RoundType const& round_type, Logger* logger);

    void stop();
};

class ServerGameHost {
    boost::optional<std::array<PlayerType, 2>> player_types;
    std::array<std::unique_ptr<Player>, 2> players;
    GameState state;

    std::uint8_t active_player = 0;

    std::atomic<bool> running {true};

    std::mutex mutating_player_list;
    std::condition_variable waiting_for_players;
    using guard = std::unique_lock<std::mutex>;

    std::unique_ptr<ServerSocketChannel<ControlObject>> controller_channel;

    std::thread control_commands_thread, game_runner_thread;

    // The mutex is only needed to synchronize the frontend and backend. We
    // want to wait for the frontend to finish the round, which we do using a
    // condition variable, which is notified from handleControlCommands.
    // This means that the entire class needs this bs mutex member, so feel
    // free to replace the whole synchronization with a cleaner mechanism if
    // you can think of any that are still portable and don't lead to
    // deadlocks, assuming that the frontend follows the protocol.
    std::mutex frontend_running_helper_mutex;
    bool frontend_round_running = false;
    std::condition_variable waiting_for_frontend_playback;

    PhysicsEngine physics_engine;

    std::unique_ptr<Logger> logger;

    void connectPlayers();

    void handleControlCommands();

public:
    ServerGameHost(std::unique_ptr<ServerSocketChannel<ControlObject>> controller_channel,
                   Map const& map,
                   std::unique_ptr<Logger> logger);

    bool registerWebsocketPlayer(std::unique_ptr<Channel<ProtocolObject>> channel, size_t player_id);

    void runGame();

    void stop();
};

class GameHost {
protected:
    std::array<std::unique_ptr<Player>, 2> players;

    GameState state;
    std::uint8_t active_player = 0;

    PhysicsEngine physics_engine;

    std::unique_ptr<Logger> logger;

    std::thread game_runner_thread;
    std::atomic<bool> running {true};

    std::pair<std::uint8_t, RoundType> detectRoundType() const;

    void executeTurn(std::uint8_t const active_player, RoundType const round_type);

public:
    virtual void stop() = 0;

    GameHost(Map const& map, std::unique_ptr<Logger> logger);
};

class HeadlessGameHost : public GameHost {
    void run();

    const unsigned max_rounds_nr = 1000;
    unsigned round_nr = 0;
    const unsigned target_score;
public:
    HeadlessGameHost(std::array<PlayerType, 2> const& player_descriptions,
                     Map const& map, std::unique_ptr<Logger> logger,
                     const unsigned target_score = std::numeric_limits<unsigned>::max());

    void stop();
    void join();
};

#endif // GAME_HPP
