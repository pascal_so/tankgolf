export const clamp = (x, minv, maxv) => {
  if (x > maxv) return maxv;
  if (x < minv) return minv;
  return x;
};
