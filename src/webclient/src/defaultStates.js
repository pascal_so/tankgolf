/* eslint-disable */

// Some default states that are used to initialize and reset
// the game controller and players.

// Use Object.assign({}, foo) instead of just assigning foo
// to make sure we get a deep copy. Without deep copy, we'd
// for example always modify both players at the same time.

const defaultTank = {
  pos: [9, 5],
  rot: 0.4
};
// This GameState is already in the format that is expected by
// the server. Anything that should not be sent to the server
// as GameState should be stored in FrontendState.
export const defaultGameState = {
  physics_state: {
    tanks: [
      Object.assign({}, defaultTank),
      Object.assign({}, defaultTank)
    ],
    bullet: [-1, -1]
  },
  scores: [0, 0]
};

// tankDeciding is used while the player is adjusting e.g.
// the barrel angle, and will be sent to the server if the
// player commits a move or sends a query.
const defaultTankDeciding = {
  barrel: 0.8,
  intensity: 0,
  x_position: 7
};
export const defaultFrontentState = {
  tanks: [
    Object.assign({}, defaultTankDeciding),
    Object.assign({}, defaultTankDeciding)
  ],
  simulated: false,
  activePlayer: 0,
  roundType: 'respawn'
};

export const defaultPressed = {
  up: false,
  down: false,
  left: false,
  right: false,
  commit: false,
  query: false
};
