import Vue from 'vue';
import Router from 'vue-router';
import PlayGame from '@/components/PlayGame';
import PlaybackRecorded from '@/components/PlaybackRecorded';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'PlayGame',
      component: PlayGame
    },
    {
      path: '/playback',
      name: 'PlaybackRecorded',
      component: PlaybackRecorded
    }
  ]
});
