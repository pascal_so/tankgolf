import {defaultPressed, defaultGameState, defaultFrontentState} from '@/defaultStates';
import {clamp} from '@/utility';
import Deque from 'double-ended-queue';

function GameController(playerDescriptions, callback, url = 'ws://localhost:9002') {
  this.gameId = -1;
  this.players = []; // players[i].controls \in {wsad, arrows, remote}
  this.controlConn = null;
  this.playerDescriptions = playerDescriptions;
  this.skipBotQueries = false;

  this.good = null;

  this.mapData = null;

  let playbackFramesQueue = null;
  let historyQueue = null;

  let serverRoundOver = false;

  let manualCommandsBlocked = false;

  let physicsStateAtRoundBeginning = null;

  // Statemachine. available states:
  // not-ready, playback, idle
  this.smState = 'not-ready';
  this.gameState = Object.assign({}, defaultGameState);
  this.frontendState = Object.assign({}, defaultFrontentState);

  this.pressedKeys = {
    wsad: Object.assign({}, defaultPressed),
    arrows: Object.assign({}, defaultPressed)
  };

  this.keydown = (ev) => { handleKey(ev, true); };
  this.keyup   = (ev) => { handleKey(ev, false); };

  const intensityChangeSpeed = 0.07;
  const barrelChangeSpeed = 0.05;
  const x_positionChangeSpeed = 0.15;

  this.tick = () => {
    if (this.smState === 'not-ready') {
      return this.getState();
    }

    // Modify frontent state, such as barrel angle
    for (let playerId = 0; playerId < this.players.length; playerId++) {
      let player = this.players[playerId]; // by reference

      // Only iterate over manually controlled tanks.
      if (player.controls === 'remote') continue;
      let tank = this.frontendState.tanks[playerId];
      let pressed = this.pressedKeys[player.controls];

      // Move x_position, barrel etc. according to keypresses
      if (this.frontendState.roundType === 'respawn' && this.frontendState.activePlayer === playerId) {
        tank.x_position += (pressed.right - pressed.left) * x_positionChangeSpeed;
        tank.x_position = clamp(tank.x_position,
          this.mapData.respawn_x_limits.start,
          this.mapData.respawn_x_limits.end);
      } else {
        // Let the player always change barrel angle, except for when they
        // are respawning.
        tank.barrel += (pressed.left - pressed.right) * barrelChangeSpeed;
        tank.barrel = clamp(tank.barrel, -Math.PI / 2, Math.PI / 2);
      }

      tank.intensity += (pressed.up - pressed.down) * intensityChangeSpeed;
      tank.intensity = clamp(tank.intensity, 0, 1);
    }

    // Check for and execute action (commit/query for shoot/respawn)
    const activePlayer = this.frontendState.activePlayer;
    const player = this.players[activePlayer];
    if (player.controls !== 'remote') {
      const tank = this.frontendState.tanks[activePlayer];
      let pressed = this.pressedKeys[player.controls];
      let msg = {
        type: ''
      };
      switch (this.frontendState.roundType) {
        case 'respawn':
          msg.x_position = tank.x_position;
          if (pressed.query) msg.type = 'queryrespawn';
          if (pressed.commit) msg.type = 'respawn';
          break;
        case 'shoot':
          msg.angle = tank.barrel;
          msg.intensity = tank.intensity;
          if (pressed.query) msg.type = 'queryshoot';
          if (pressed.commit) msg.type = 'shoot';
          break;
      }
      if (msg.type !== '') {
        // debounce keys
        pressed.query = false;
        pressed.commit = false;
        manualCommandsBlocked = true;

        player.conn.send(JSON.stringify(msg));
      }
    }

    // display tank position from history playback
    if (this.smState === 'playback' && !playbackFramesQueue.isEmpty()) {
      this.gameState.physics_state = playbackFramesQueue.dequeue();
      if (playbackFramesQueue.isEmpty()) {
        this.smState = 'idle';
        if (this.frontendState.simulated) {
          this.gameState.physics_state = physicsStateAtRoundBeginning;
        }
        this.frontendState.simulated = false;
        manualCommandsBlocked = false;
      }
    }

    // handle history
    while (!historyQueue.isEmpty() && this.smState === 'idle') {
      const histObj = historyQueue.dequeue();
      switch (histObj.event.type) {
        case 'motion':
          if (histObj.simulated && this.skipBotQueries && player.controls === 'remote') {
            break;
          }

          for (let i = 0; i < histObj.event.trajectory.length; i++) {
            playbackFramesQueue.enqueue(histObj.event.trajectory[i]);
          }
          this.frontendState.simulated = histObj.simulated;
          this.smState = 'playback';
          break;
        case 'fall_off_map':
          if (!histObj.simulated) {
            this.gameState.scores[1 - histObj.event.player_id]++;
          }
          break;
        case 'shoot':
          this.frontendState.tanks[histObj.active_player_id].barrel = histObj.event.angle;
          this.frontendState.tanks[histObj.active_player_id].intensity = histObj.event.intensity;
          break;
        case 'impact':
          // TODO: display explosion animation
          // impact pos: (histObj.pos[0], histObj.pos[1])
          break;
      }
    }

    // Overwrite displayed tank position if currently respawning.
    if (this.frontendState.roundType === 'respawn' && this.smState === 'idle') {
      const activePlayer = this.frontendState.activePlayer;
      this.gameState.physics_state.tanks[activePlayer].pos = [
        this.frontendState.tanks[activePlayer].x_position,
        this.mapData.respawn_height
      ];
    }

    // Notify server if playback is finished, so that thee server can start
    // sending data for the next round.
    if (serverRoundOver &&
        historyQueue.isEmpty() &&
        playbackFramesQueue.isEmpty()) {
      this.controlConn.send(JSON.stringify({type: 'finished_playback'}));
      serverRoundOver = false;
    }

    return this.getState();
  };

  this.getState = () => {
    return {
      gameState: this.gameState,
      frontendState: this.frontendState,
      smState: this.smState
    };
  };

  const stop = () => {
    historyQueue.clear();
    playbackFramesQueue.clear();
    this.smState = 'idle';

    this.controlConn.close();
    this.controlConn = null;
    for (let i = 0; i < this.players.length; i++) {
      if (this.players[i].type !== 'remote') {
        this.players[i].conn.close();
        this.players[i].conn = null;
      }
    }
  };

  this.stopGame = () => {
    this.controlConn.send(JSON.stringify({type: 'stop_game'}));
    stop();
  };

  this.stopServer = () => {
    this.controlConn.send(JSON.stringify({type: 'stop_server'}));
    stop();
  };

  const startPlayers = () => {
    for (let i = 0; i < this.playerDescriptions.length; i++) {
      let player = null;

      if (this.playerDescriptions[i].type === 'websocket') {
        player = {
          conn: new WebSocket(url, 'player'),
          controls: this.playerDescriptions[i].controls
        };

        player.conn.onopen = () => {
          player.conn.send(`${this.gameId} ${i}`);
        };
        player.conn.onmessage = (msg) => {
          onPlayerMessage(msg.data, i);
        };
        player.conn.onerror = (err) => {
          console.log(`Player ${i} error: ${err}`);
        };
        player.conn.onclose = (msg) => {
          console.log(`Player ${i} close: ${msg}`);
        };
      } else {
        player = { controls: 'remote' };
      }

      this.players.push(player);
    }
  };

  const onPlayerMessage = (msg, playerId) => {
    const obj = JSON.parse(msg);

    if (obj.error) {
      console.log(obj.error);
      return;
    }

    switch (obj.type) {
      case 'startshoot':
        break;
      case 'startrespawn':
        break;
      case 'state':
        break;
    }
  };

  const onControlMessage = msg => {
    if (this.gameId === -1) {
      // The first message by the server is the game id.
      this.gameId = parseInt(msg);
      let player_descriptions_msg = JSON.stringify({
        type: 'player_descriptions',
        players: playerDescriptions
      });
      console.log('sending player descriptions:', player_descriptions_msg);
      this.controlConn.send(player_descriptions_msg);
      startPlayers();
      this.smState = 'idle';
      return;
    }

    switch (msg.type) {
      case 'history':
        const history = msg.history;
        for (let i = 0; i < history.length; i++) {
          historyQueue.enqueue(history[i]);
        }
        break;
      case 'start_round':
        this.frontendState.roundType = msg.round_type;
        this.frontendState.activePlayer = msg.player_id;
        this.gameState = msg.state;
        physicsStateAtRoundBeginning = msg.state.physics_state;
        break;
      case 'end_round':
        serverRoundOver = true;
        break;
    }
  };

  const handleKey = (ev, pressed) => {
    let keyIsRelevant = true;
    switch (ev.key) {
      // arrows
      case 'ArrowUp':
        this.pressedKeys.arrows.up = pressed;
        break;
      case 'ArrowDown':
        this.pressedKeys.arrows.down = pressed;
        break;
      case 'ArrowLeft':
        this.pressedKeys.arrows.left = pressed;
        break;
      case 'ArrowRight':
        this.pressedKeys.arrows.right = pressed;
        break;
      case 'Enter':
        if (!manualCommandsBlocked || !pressed) {
          this.pressedKeys.arrows.commit = pressed;
        }
        break;
      case ' ':
        if (!manualCommandsBlocked || !pressed) {
          this.pressedKeys.arrows.query = pressed;
        }
        break;

      // wsad
      case 'w':
        this.pressedKeys.wsad.up = pressed;
        break;
      case 's':
        this.pressedKeys.wsad.down = pressed;
        break;
      case 'a':
        this.pressedKeys.wsad.left = pressed;
        break;
      case 'd':
        this.pressedKeys.wsad.right = pressed;
        break;
      case 'e':
        if (!manualCommandsBlocked || !pressed) {
          this.pressedKeys.wsad.commit = pressed;
        }
        break;
      case 'q':
        if (!manualCommandsBlocked || !pressed) {
          this.pressedKeys.wsad.query = pressed;
        }
        break;

      default:
        // Make sure that the app still allows e.g. ctrl+T to be
        // acted on by the browser.
        keyIsRelevant = false;
    }

    if (keyIsRelevant) {
      ev.preventDefault();
    }
  };

  const init = (callback) => {
    this.controlConn = new WebSocket(url, 'control');
    this.controlConn.onmessage = msg => {
      onControlMessage(JSON.parse(msg.data));
    };
    this.controlConn.onerror = error => {
      console.log('Connection to Game Server failed.', error);
      if (this.good !== true) {
        this.good = false;
        callback(this.good);
      }
    };
    // Sending the first message to the server, content is irrelevant.
    this.controlConn.onopen = () => {
      this.controlConn.send('hi');
      this.good = true;
      callback(this.good);
    };
    this.controlConn.onclose = ev => {
      console.log('Connection to Game Server closed.', ev);
    };

    playbackFramesQueue = new Deque();
    historyQueue = new Deque();
  };

  // TODO: maybe solve this with a JS Promise instead..
  init(callback);
}

export default GameController;
