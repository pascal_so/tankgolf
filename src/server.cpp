#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#include "channel.hpp"
#include "server.hpp"
#include "types/control_object_types.hpp"
#include "util/util.hpp"

GamesManager::GamesManager() : next_id(0) {}

int GamesManager::addGame(
    ws_server* server, connection_hdl control_connection_hdl,
    std::shared_ptr<blocking_queue<OutgoingMessage>> game_to_browser,
    std::shared_ptr<blocking_queue<std::string>> browser_to_game) {
    guard g(mutex);
    int game_id = ++next_id;

    std::cerr << "Creating game with id " << game_id << ".\n";

    auto control_channel = std::make_unique<ServerSocketChannel<ControlObject>>(
        formatJsonControlObject,
        parseJsonControlObject,
        game_to_browser, browser_to_game,
        control_connection_hdl, server);

    std::vector<std::unique_ptr<Logger>> loggers;
    loggers.emplace_back(new FileLogger("game.log"));
    loggers.emplace_back(new NetworkLogger(game_to_browser, control_connection_hdl));
    std::unique_ptr<Logger> logger(new CombinedLogger(std::move(loggers)));

    // TODO: move this to main file, maybe take path to map as command line argument?
    // The current solution is a bit flaky as we're relying on a relative path and thus
    // the location of the binary.
    // Also there's no reason to create more than one instance of the map object.
    Map map("../geometry/maps/map1.json");
    auto host = std::make_unique<ServerGameHost>(std::move(control_channel), map, std::move(logger));

    active_games[game_id] = std::move(host);

    return game_id;
}

void GamesManager::stopGame(int id) {
    guard g(mutex);

    if (active_games.find(id) == active_games.end()) {
        return;
    }

    active_games[id]->stop();
    active_games.erase(id);
}

void GamesManager::stopAllGames() {
    guard g(mutex);
    for (auto& game : active_games) {
        game.second->stop();
    }

    active_games.clear();
}

size_t GamesManager::size() {
    guard g(mutex);
    return active_games.size();
}

ServerGameHost* GamesManager::getGameById(int id) {
    guard g(mutex);

    if (active_games.find(id) == active_games.end()) {
        return nullptr;
    }

    return active_games[id].get();
}

GameServer::GameServer()
    : m_game_to_browser{std::make_unique<blocking_queue<OutgoingMessage>>()} {
    m_running = true;

    m_server.init_asio();

    m_server.set_open_handler(std::bind(&GameServer::on_open, this, std::placeholders::_1));
    m_server.set_close_handler(std::bind(&GameServer::on_close, this, std::placeholders::_1));
    m_server.set_message_handler(
        std::bind(&GameServer::on_message, this, std::placeholders::_1, std::placeholders::_2));
    m_server.set_validate_handler(
        std::bind(&GameServer::on_validate, this, std::placeholders::_1));

    m_server.set_reuse_addr(true);
}

bool GameServer::on_validate(connection_hdl hdl) {
    auto con = m_server.get_con_from_hdl(hdl);

    auto protocols = con->get_requested_subprotocols();

    if (protocols.empty()) {
        // reject
        std::cerr << "A websocket client tried to connect without specified "
                     "subprotocol. Rejected.\n";
        con->set_status(websocketpp::http::status_code::ok);
        return false;
    }

    std::string protocol = protocols[0];

    if (protocol == "control") {
        con->role = ConnectionRole::GameController;
    } else if (protocol == "player") {
        con->role = ConnectionRole::Player;
    } else {
        // unknown protocol
        std::cerr
            << "A websocket client tried to connect with unknown subprotocol '"
            << protocol << "'. Rejected.\n";
        return false;
    }

    con->select_subprotocol(protocol);

    return true;
}

void GameServer::on_open(connection_hdl hdl) {
    std::lock_guard<std::mutex> lock(m_mtx_floating_connections);
    m_floating_connections.insert(hdl);
}

void GameServer::on_close(connection_hdl hdl) {
    auto con = m_server.get_con_from_hdl(hdl);

    int id = con->game_id;

    if (id == -1) {
        // couldn't care less
        return;
    }

    if (con->role == ConnectionRole::GameController) {
        m_games_manager.stopGame(id);
    }
}

void GameServer::on_message(connection_hdl hdl, ws_server::message_ptr msg) {
    auto con = m_server.get_con_from_hdl(hdl);

    if (con->game_id == -1) {
        switch (con->role) {
        case ConnectionRole::GameController:
            con->game_id = m_games_manager.addGame(
                &m_server, hdl, m_game_to_browser, con->received_messages);

            // Note that we're ignoring the first message received by the
            // controller, instead we just start a new game and send back
            // the assigned game_id.

            m_game_to_browser->push({hdl, std::to_string(con->game_id)});

            break;
        case ConnectionRole::Player:
            std::string payload = msg->get_payload();
            std::stringstream ss(payload);

            size_t player_id;
            ss >> con->game_id >> player_id;

            if (ss.fail()) {
                std::cerr
                    << "Websocket player tried to connect with invalid query :'"
                    << payload << "'.\n";
                break;
            }

            if (player_id != 0 && player_id != 1) {
                std::cerr
                    << "Websocket player tried to connect with invalid player id :"
                    << player_id << ". Only 0 and 1 allowed.\n";
                break;
            }

            auto host = m_games_manager.getGameById(con->game_id);

            if (!host) {
                std::cerr
                    << "Websocket player tried to connect to invalid game id: "
                    << con->game_id << ".\n";
                m_server.pause_reading(hdl);
                m_server.close(hdl, websocketpp::close::status::normal, "");
                break;
            }

            auto parser = [](std::istream& stream){ return parseJsonProtocolObject(Util::readJson(stream)); };
            auto channel =
                std::make_unique<ServerSocketChannel<ProtocolObject>>(
                    formatJsonProtocol, parser,
                    m_game_to_browser, con->received_messages, hdl,
                    &m_server);

            bool register_player_success =
                host->registerWebsocketPlayer(std::move(channel), player_id);

            if (!register_player_success) {
                std::cerr
                    << "Websocket player tried to connect to game " << con->game_id
                    << " as player " << player_id << ". Rejected by host.\n";
                break;
            }


            std::cerr << "Successfully connected a websocket player to game "
                      << con->game_id << " as player " << player_id << ".\n";

            break;
        }

        {
            std::lock_guard<std::mutex> lock(m_mtx_floating_connections);
            m_floating_connections.erase(hdl);
        }
    } else {
        con->received_messages->push(msg->get_payload());
    }
}

void GameServer::process_outgoing_messages() {
    // runs in separate thread

    std::cerr << "Processing outgoing messages.\n";
    while (m_running) {
        auto outgoing = m_game_to_browser->pop();

        if (outgoing.text != "") {
            // std::cerr << "WebSocket server sending message: " << outgoing.text << '\n';
            m_server.send(outgoing.hdl, outgoing.text,
                          websocketpp::frame::opcode::text);
        }
    }
}

void GameServer::run(uint16_t port) {
    std::cerr << "Running websocket server on port " << port << ".\n";

    // websocket server runs in 2 threads, one for opening/closing connections
    // and receiving messages, one for sending messages.

    std::thread send_thread(
        std::bind(&GameServer::process_outgoing_messages, this));

    m_server.clear_access_channels( websocketpp::log::alevel::frame_header |
                                    websocketpp::log::alevel::frame_payload);

    m_server.listen(port);
    m_server.start_accept();
    m_server.run();

    send_thread.join();
}

void GameServer::stop() {
    std::cerr << "Quitting..\n";

    m_running = false;
    m_server.stop_listening();

    // put memes in the outgoing queue to stop it from blocking
    m_game_to_browser->push({{}, ""});

    m_games_manager.stopAllGames();
    {
        std::lock_guard<std::mutex> lock(m_mtx_floating_connections);
        for (auto& hdl : m_floating_connections) {
            m_server.pause_reading(hdl);
            m_server.close(hdl, websocketpp::close::status::normal, "");
        }
    }
}
