#ifndef CUSTOM_WEBSOCKET_CONFIG_HPP
#define CUSTOM_WEBSOCKET_CONFIG_HPP

#include <memory>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

#include "util/blocking_queue.hpp"

enum class ConnectionRole { GameController, Player };

// This is the custom data carried around by every websocket connection.
struct ConnectionData {
    ConnectionRole role;

    int game_id;

    std::shared_ptr<blocking_queue<std::string>> received_messages;

    ConnectionData();
};

struct custom_websocket_config : public websocketpp::config::asio {
    // pull default settings from our core config
    typedef websocketpp::config::asio core;

    typedef core::concurrency_type concurrency_type;
    typedef core::request_type request_type;
    typedef core::response_type response_type;
    typedef core::message_type message_type;
    typedef core::con_msg_manager_type con_msg_manager_type;
    typedef core::endpoint_msg_manager_type endpoint_msg_manager_type;
    typedef core::alog_type alog_type;
    typedef core::elog_type elog_type;
    typedef core::rng_type rng_type;
    typedef core::transport_type transport_type;
    typedef core::endpoint_base endpoint_base;

    // Set a custom connection_base class
    typedef ConnectionData connection_base;
};

typedef websocketpp::server<custom_websocket_config> ws_server;

using websocketpp::connection_hdl;

struct OutgoingMessage {
    connection_hdl hdl;
    std::string text;
};

#endif // CUSTOM_WEBSOCKET_CONFIG_HPP
