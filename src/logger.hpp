#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <vector>
#include <fstream>

#include "types/history_object_types.hpp"
#include "custom_websocket_config.hpp"
#include "util/blocking_queue.hpp"

class Logger {
    virtual void log_internal(std::vector<History::HistoryObject> const& objs) = 0;
public:
    bool flipped = false;
    void log(std::vector<History::HistoryObject> objs);
    virtual void close();
};

class FileLogger : public Logger {
    std::ofstream file;

    void log_internal(std::vector<History::HistoryObject> const& objs) override;
public:
    FileLogger(std::string const& filename);

    void close() override;
};

class NetworkLogger : public Logger {
    std::shared_ptr<blocking_queue<OutgoingMessage>> queue;
    connection_hdl hdl;

    void log_internal(std::vector<History::HistoryObject> const& objs) override;
public:
    NetworkLogger(std::shared_ptr<blocking_queue<OutgoingMessage>> queue,
                          connection_hdl const& hdl);
};

class CombinedLogger : public Logger {
    std::vector<std::unique_ptr<Logger>> backends;

    void log_internal(std::vector<History::HistoryObject> const& objs) override;
public:
    CombinedLogger(std::vector<std::unique_ptr<Logger>> backends);

    void close() override;
};

#endif // LOGGER_HPP
