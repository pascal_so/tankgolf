#include "game.hpp"
#include "util/lambda_visitor.hpp"
#include "util/util.hpp"

ShootCommit addShotInaccuracy(ShootCommit command, std::mt19937& gen) {
    // intensity goes from 0 to 1, angle from -pi/2 to +pi/2

    const float max_uncertainty = M_PI / 9.0; // 20°
    const float uncertainty_radius = max_uncertainty * command.intensity * command.intensity;
    std::uniform_real_distribution<float> dist (-1, 1);

    const float new_angle = command.angle + dist(gen) * uncertainty_radius;

    command.angle = Util::clamp(new_angle, (float)-M_PI/2, (float)M_PI/2);

    return command;
}

Player::Player(std::unique_ptr<Channel<ProtocolObject>> channel, PhysicsEngine* engine, PlayerType const& type)
    : channel(std::move(channel)),
      physics_engine(engine),

#ifdef _WIN32
      rng(1),
#else
      // It wouldn't make sense to seed this deterministically, as the physics simulation is not deterministic
      // (depends on the floting point maths of the processor) anyway. The rng is used to offset the shooting
      // angle.
      rng(std::random_device()()),
#endif

      type(type)
{}


std::vector<History::HistoryObject> Player::handleQuery(ProtocolObject const& po, TankOrientations const& tank_orientations) {
    std::vector<History::HistoryObject> events;

    apply_lambda_visitor<void>(
        po,
        [&](QueryShoot const& query){
            events = physics_engine->handleAction(tank_orientations, 0, true,
                                                   ShootCommit{query.angle, query.intensity});
        },
        [&](QueryRespawn const& query){
            events = physics_engine->handleAction(tank_orientations, 0, true,
                                                   RespawnCommit{query.x_position});
        },
        [&](FullQueryShoot const& query){
            events = physics_engine->handleAction(query.state, 0, true,
                                                   ShootCommit{query.angle, query.intensity});
        },
        [&](FullQueryRespawn const& query){
            events = physics_engine->handleAction(query.state, 0, true,
                                                   RespawnCommit{query.x_position});
        },
        [](auto const&){
            throw std::invalid_argument("handleQuery called with non-query ProtocolObject.");
        }
    );

    return events;
}

std::pair<std::vector<History::HistoryObject>, GameState>
Player::turn(const GameState& state, RoundType const& round_type, Logger* logger) {
    switch (round_type) {
    case RoundType::Shoot:
        channel->send(ShootRoundStart{state});
        break;
    case RoundType::Respawn:
        channel->send(RespawnRoundStart{state});
        break;
    }

    std::vector<History::HistoryObject> events;
    GameState resulting_state;

    while (true) {
        auto command = channel->receive();

#ifdef TANKGOLF_VERBOSE
        std::cerr << "Received " << strProtocolObjectName(command) << " from player\n";
#endif

        // this section here requires RTTI, so maybe we have to think of something nicer..

        if (isQuery(command)) {
            std::vector<History::HistoryObject> new_events = handleQuery(command, state.physics_state.tanks);
            resulting_state = state;
            resulting_state.physics_state = physics_engine->getPhysicsStateImpact();
            auto scores = History::getScores(new_events, true);
            for (std::size_t i = 0; i < resulting_state.scores.size(); ++i) {
                resulting_state.scores[i] += scores[i];
            }
            channel->send(resulting_state);

            logger->log(new_events);
            Util::append(events, new_events);
        } else if (round_type == RoundType::Shoot && command.type() == typeid(ShootCommit)){
            ShootCommit shoot_commit = addShotInaccuracy(boost::get<ShootCommit>(command), rng);

            std::vector<History::HistoryObject> new_events =
                physics_engine->handleAction(state.physics_state.tanks, 0, false, shoot_commit);

            logger->log(new_events);
            Util::append(events, new_events);
            break;
        } else if (round_type == RoundType::Respawn && command.type() == typeid(RespawnCommit)) {
            RespawnCommit respawn_commit = boost::get<RespawnCommit>(command);

            std::vector<History::HistoryObject> new_events =
                physics_engine->handleAction(state.physics_state.tanks, 0, false, respawn_commit);

            logger->log(new_events);
            Util::append(events, new_events);
            break;
        } else {
            throw std::invalid_argument("Invalid response type: " + formatStringProtocol(command) +
                                        " (round type: " +
                                        (round_type == RoundType::Shoot ? "shoot" : "respawn") + ")");
        }
    }

    resulting_state = state;
    resulting_state.physics_state = physics_engine->getPhysicsStateImpact();
    auto scores = History::getScores(events);
    for (std::size_t i = 0; i < resulting_state.scores.size(); ++i) {
        resulting_state.scores[i] += scores[i];
    }
    return {events, resulting_state};
}

void Player::stop() { channel->kill(); }

ServerGameHost::ServerGameHost(
    std::unique_ptr<ServerSocketChannel<ControlObject>> controller_channel,
    Map const& map,
    std::unique_ptr<Logger> logger)
    : controller_channel(std::move(controller_channel)),
      control_commands_thread(std::bind(&ServerGameHost::handleControlCommands, this)),
      game_runner_thread(std::bind(&ServerGameHost::runGame, this)),
      physics_engine(map),
      logger(std::move(logger)) {}


void ServerGameHost::connectPlayers() {
    guard g(mutating_player_list);

    for (size_t id : {0, 1}) {
        if (players[id] != nullptr) {
            continue;
        }

        apply_lambda_visitor<void>(
            (*player_types)[id],
            [&](PlayerTypes::Websocket const&){
                // ignore, this is handled by registerWebsocketPlayer
            },
            [&](PlayerTypes::Server const&){
                // todo
            },
            [&](PlayerTypes::Bot const& pt){
                auto channel = std::make_unique<ProcessChannel<ProtocolObject>>
                    (pt.command, formatStringProtocol, parseStringProtocolObject);

                auto player = std::make_unique<Player>(std::move(channel), &physics_engine, pt);

                players[id] = std::move(player);
            }
        );
    }

    waiting_for_players.notify_one();
}

// The ServerGameHost can't initiate the websocket connection on its
// own, it is handed over from the server. Other connections are
// opened in connectPlayers()
bool ServerGameHost::registerWebsocketPlayer(std::unique_ptr<Channel<ProtocolObject>> channel,
                                             size_t player_id) {
    guard g(mutating_player_list);

    assert(player_id == 0 || player_id == 1);

    if (players[player_id] != nullptr) {
        // already registered this player id
        return false;
    }

    if (!player_types || (*player_types)[player_id].which() != 0) {
        // player list not defined yet or wrong player type
        return false;
    }

    players[player_id] = std::make_unique<Player>(std::move(channel),
                                                  &physics_engine,
                                                  PlayerTypes::Websocket());

    waiting_for_players.notify_one();

    return true;
}

void ServerGameHost::handleControlCommands() {
    while (running) {
        auto cmd = controller_channel->receive();

        apply_lambda_visitor<void>(
            cmd,
            [&](ControlObjectTypes::PlayerList const& pl){
                std::cerr << "Received PlayerList.\n";
                {
                    guard g(mutating_player_list);

                    if (player_types) {
                        std::cerr
                            << "Warning: ServerGameHost already received player types. Ignoring.\n";
                        return;
                    }

                    player_types = pl.players;
                }
                connectPlayers();
            },
            [&](ControlObjectTypes::StopGame const&){
                stop();
            },
            [&](ControlObjectTypes::StopServer const&){
                stop();
                // todo: stop entire game server
            },
            [&](ControlObjectTypes::FinishedPlayback const&){
                guard g(frontend_running_helper_mutex);
                assert(frontend_round_running);
                frontend_round_running = false;
                waiting_for_frontend_playback.notify_one();
            },
            [&](auto const&){
                std::cerr
                    << "ERROR: Received unexpected object in ServerGameHost.\n";
                assert(false);
            }
        );
    }
}

void ServerGameHost::runGame() {
    {
        guard g(mutating_player_list);

        std::cerr << "ServerGameHost: Waiting for players to be registered.\n";
        waiting_for_players.wait(g, [&]{
                return players[0] != nullptr && players[1] != nullptr;
            });
    }

    while (running) {
        RoundType round_type;
        std::uint8_t player_id = active_player;
        round_type = RoundType::Shoot;

        for (auto i : {0, 1}) {
            if (state.physics_state.tanks[i].pos == point{-1, -1}) {
                round_type = RoundType::Respawn;
                player_id = i;
                break;
            }
        }

        {
            guard g(frontend_running_helper_mutex);
            assert(!frontend_round_running);
            frontend_round_running = true;
        }
        controller_channel->send(ControlObjectTypes::StartRound {round_type, player_id, state});

        std::cerr << "Starting round for player " << (char)(player_id + 'A') << " type "
                  << (round_type == RoundType::Shoot ? "shoot" : "respawn") << '\n';

        auto player_visible_state = state;
        logger->flipped = false;
        if (player_id != 0) {
            flipPlayers(player_visible_state);
            logger->flipped = true;
        }

        using HistObj = History::HistoryObject;
        std::vector<HistObj> history;
        GameState newState;

        try {
            std::tie(history, newState) = players[player_id]->turn(player_visible_state,
                                                                   round_type,
                                                                   logger.get());
        } catch (std::exception const& e){
            std::cerr << "Error in turn of player " << (char)(player_id + 'A')
                      << ": " << e.what() << "\n";
            stop();
            break;
        }

        if (player_id != 0) {
            flipPlayers(history);
            flipPlayers(newState);
            logger->flipped = false;
        }

        state = newState;
        std::cerr << "New scores: "
                  << state.scores[0] << ' '
                  << state.scores[1] << '\n';

        if (round_type == RoundType::Shoot) {
            active_player ^= 1;
        }

        controller_channel->send(ControlObjectTypes::EndRound {});
        {
            // Wait for frontend to finish playback
            guard g(frontend_running_helper_mutex);
            waiting_for_frontend_playback.wait(g, [this]{
                    return !frontend_round_running;
                });
        }
    }
}

void ServerGameHost::stop() {
    if (controller_channel) {
        controller_channel->kill();
        controller_channel.reset();
    }

    running = false;

    {
        guard g(mutating_player_list);

        for (auto& p : players) {
            if (p) {
                p->stop();
                p.reset();
            }
        }
    }

    control_commands_thread.join();
    game_runner_thread.join();
}

GameHost::GameHost(Map const& map, std::unique_ptr<Logger> logger)
    : physics_engine(map), logger(std::move(logger)) {}

HeadlessGameHost::HeadlessGameHost(std::array<PlayerType, 2> const& player_descriptions,
                                   Map const& map, std::unique_ptr<Logger> logger,
                                   const unsigned target_score)
    : GameHost(map, std::move(logger)), target_score(target_score)
{
    for (std::size_t id = 0; id < 2; ++id) {
        apply_lambda_visitor<void>(
            player_descriptions[id],
            [&](PlayerTypes::Websocket const&){
                throw std::invalid_argument("Manual player not supported for HeadlessGameHost.");
            },
            [&](PlayerTypes::Server const&){
                // we don't need this.
            },
            [&](PlayerTypes::Bot const& bot){
                auto channel = std::make_unique<ProcessChannel<ProtocolObject>>
                    (bot.command, formatStringProtocol, parseStringProtocolObject);
                players[id] = std::make_unique<Player>(std::move(channel), &physics_engine, bot);
            }
        );
    }

    game_runner_thread = std::thread(std::bind(&HeadlessGameHost::run, this));
}

void HeadlessGameHost::run() {
    while (this->running &&
           state.scores[0] < target_score &&
           state.scores[1] < target_score ) {
        RoundType round_type;
        std::uint8_t player_id;
        std::tie(player_id, round_type) = detectRoundType();

        executeTurn(player_id, round_type);

        if (round_type == RoundType::Shoot) {
            active_player ^= 1;
        }

        if (round_nr++ >= max_rounds_nr) {
            break;
        }
    }

    std::cout << state.scores[0] << ' ' << state.scores[1] << '\n';

    for (auto& player : players) {
        player->stop();
    }

    logger->close();
}

void HeadlessGameHost::stop() {
    running = false;
    this->join();
}

void HeadlessGameHost::join() {
    game_runner_thread.join();
}

std::pair<std::uint8_t, RoundType> GameHost::detectRoundType() const {
    RoundType round_type = RoundType::Shoot;;
    std::uint8_t player_id = active_player;

    for (std::uint8_t id = 0; id < 2; ++id) {
        if (state.physics_state.tanks[id].pos == point{-1, -1}) {
            round_type = RoundType::Respawn;
            player_id = id;
            break;
        }
    }

    return {player_id, round_type};
}

void GameHost::executeTurn(std::uint8_t const active_player, RoundType const round_type) {
    std::cerr << "Starting round for player " << (char)(active_player + 'A') << " type "
              << (round_type == RoundType::Shoot ? "shoot" : "respawn") << '\n';

    auto player_visible_state = state;
    logger->flipped = false;
    if (active_player != 0) {
        flipPlayers(player_visible_state);
        logger->flipped = true;
    }

    using HistObj = History::HistoryObject;
    std::vector<HistObj> history;
    GameState newState;

    try {
        std::tie(history, newState) = players[active_player]->turn(player_visible_state,
                                                                   round_type,
                                                                   logger.get());
    } catch (std::exception const& e){
        std::cerr << "Error in turn of player " << (char)(active_player + 'A')
                  << ": " << e.what() << "\n";
        if (running)
            stop();
        return;
    }

    if (active_player != 0) {
        flipPlayers(history);
        flipPlayers(newState);
        logger->flipped = false;
    }

    state = newState;
    std::cerr << "New scores: "
              << state.scores[0] << ' '
              << state.scores[1] << '\n';
}
