#include <cmath>
using std::isfinite;

// Box2D accesses std::isfinite without std, and this works in some versions of
// g++ and in others it doesn't and I have no clue why.. but this fixes it so I
// don't care.
