#ifndef COMMUNICATION_HPP
#define COMMUNICATION_HPP

#include <boost/process.hpp>
#include <functional>
#include <exception>

#include "custom_websocket_config.hpp"
#include "types/protocol_object_types.hpp"
#include "util/blocking_queue.hpp"


template <typename T> class Channel {
public:
    virtual void send(T) = 0;

    virtual T receive() = 0;

    virtual void kill() = 0;
};

// todo implement client socket channel to communicate with closed-source server
// bot
template <typename T> class ClientSocketChannel : public Channel<T> {
    std::function<std::string(T)> formatter;
    std::function<T(std::istream&)> parser;

public:
    void send(T obj) { std::string out = formatter(obj); }

    T receive() {
        // return parser(std::stringstream(""));
    }

    void kill() {}
};

template <typename T> class ServerSocketChannel : public Channel<T> {
    std::function<std::string(T)> formatter;
    std::function<T(std::istream&)> parser;

    std::shared_ptr<blocking_queue<OutgoingMessage>> game_to_browser;
    std::shared_ptr<blocking_queue<std::string>> browser_to_game;

    connection_hdl hdl;
    ws_server* server;

    std::atomic<bool> running;

public:
    ServerSocketChannel(
        std::function<std::string(T)> formatter,
        std::function<T(std::istream&)> parser,
        std::shared_ptr<blocking_queue<OutgoingMessage>> game_to_browser,
        std::shared_ptr<blocking_queue<std::string>> browser_to_game,
        connection_hdl hdl, ws_server* server)
        : formatter(std::move(formatter)), parser(std::move(parser)),
          game_to_browser(game_to_browser), browser_to_game(browser_to_game),
          hdl(hdl), server(server), running(true) {}

    void send(T obj) { game_to_browser->push({hdl, formatter(obj)}); }

    T receive() {
        std::stringstream received(browser_to_game->pop());

        if (!running) {
            throw std::invalid_argument("Connection not active.");
        }

        return parser(received);
    }

    void kill() {
        running = false;
        browser_to_game->push("");
        try {
            server->pause_reading(hdl);
            server->close(hdl, websocketpp::close::status::normal, "");
        } catch (std::exception const& e) {
            std::cerr << "Exception while killing websocket connection: " << e.what() << '\n';
        }
    }
};

template <typename T> class ProcessChannel : public Channel<T> {
    boost::process::group child_process_group;
    boost::process::child child_process;
    boost::process::pstream out_stream;
    boost::process::pstream in_stream;

    std::function<std::string(ProtocolObject)> formatter;
    std::function<T(std::istream&)> parser;

public:
    ProcessChannel(
        std::string command,
        std::function<std::string(ProtocolObject)> formatter,
        std::function<T(std::istream&)> parser)
        : formatter(std::move(formatter)), parser(std::move(parser)) {

#ifdef TANKGOLF_VERBOSE
        std::cerr << "Starting boost process with command: \"" << command << "\"\n";
#endif

        child_process = boost::process::child(
            command,
            boost::process::std_err > stderr,
            boost::process::std_out > out_stream,
            boost::process::std_in  < in_stream,
            child_process_group);
    }

    void send(T obj) {
        if (child_process.running()) {
            const std::string message = formatter(obj);
#ifdef TANKGOLF_VERBOSE
            std::cerr << "Sending string " << message << '\n';
#endif
            in_stream << message << std::endl;
        }
    }

    T receive() {
        if (!child_process.running()) {
            throw std::invalid_argument("Bot not running.");
        }

        const T parsed = parser(out_stream);
        return parsed;
    }

    void kill() {
        if (child_process.running()) {
            try {
                child_process_group.terminate();
            } catch (boost::process::process_error const& e) {
                // This probably just tells us that the process
                // has already been killed. We don't care.
            }
        }
    }
};

#endif // COMMUNICATION_HPP
