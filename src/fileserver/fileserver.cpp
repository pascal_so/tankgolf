#include "fileserver.hpp"

#include <algorithm>
#include <fstream>
#include <vector>
#include <exception>

// Serve local files over HTTP. This code is mostly copy pasted from the Simple-Web-Server example.

FileServer::FileServer(std::string const& web_root, std::uint16_t port)
    : web_root_path(boost::filesystem::canonical(web_root)) {
    server.config.port = port;

    server.default_resource["GET"] = [this](std::shared_ptr<HttpServer::Response> response, std::shared_ptr<HttpServer::Request> request) {
        try {
            auto path = boost::filesystem::canonical(web_root_path / request->path);
            // Check if path is within web_root_path
            if(std::distance(web_root_path.begin(), web_root_path.end()) > std::distance(path.begin(), path.end()) ||
                 !std::equal(web_root_path.begin(), web_root_path.end(), path.begin()))
                throw std::invalid_argument("path must be within root path");
            if(boost::filesystem::is_directory(path))
                path /= "index.html";

            SimpleWeb::CaseInsensitiveMultimap header;

            header.emplace("Cache-Control", "max-age=86400");

            auto ifs = std::make_shared<std::ifstream>();
            ifs->open(path.string(), std::ifstream::in | std::ios::binary | std::ios::ate);

            if(*ifs) {
                auto length = ifs->tellg();
                ifs->seekg(0, std::ios::beg);

                header.emplace("Content-Length", to_string(length));
                response->write(header);

                // Don't hate on me this code is part of the Simple-Web-Server example...
                // Trick to define a recursive function within this scope.
                class FileServerRecursive {
                public:
                    static void read_and_send(const std::shared_ptr<HttpServer::Response> &response, const std::shared_ptr<std::ifstream> &ifs) {
                        // Read and send 128 KB at a time
                        static std::vector<char> buffer(131072); // Safe when server is running on one thread
                        std::streamsize read_length = ifs->read(&buffer[0], static_cast<std::streamsize>(buffer.size())).gcount();
                        if (read_length > 0) {
                            response->write(&buffer[0], read_length);
                            if (read_length == static_cast<std::streamsize>(buffer.size())) {
                                response->send([response, ifs](const SimpleWeb::error_code &ec) {
                                    if (!ec)
                                        read_and_send(response, ifs);
                                    else
                                        std::cerr << "FileServer: Connection interrupted\n.";
                                });
                            }
                        }
                    }
                };
                FileServerRecursive::read_and_send(response, ifs);
            }
            else
                throw std::invalid_argument("could not read file");
        }
        catch(const std::exception &e) {
          response->write(SimpleWeb::StatusCode::client_error_bad_request, "Could not open path " + request->path + ": " + e.what());
        }
    };

    server.on_error = [](std::shared_ptr<HttpServer::Request> /*request*/, const SimpleWeb::error_code & ec) {
        // Handle errors here
        // Note that connection timeouts will also call this handle with ec set to SimpleWeb::errc::operation_canceled

        std::cerr << "Error in FileServer. " << ec << '\n';
    };
}

void FileServer::start() { server.start(); }
void FileServer::stop() { server.stop(); }
