#ifndef FILESERVER_HPP
#define FILESERVER_HPP

#include <server_http.hpp>
#include <boost/filesystem.hpp>
#include <string>

class FileServer {
    using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;

    HttpServer server;
    const boost::filesystem::path web_root_path;

public:
    FileServer(std::string const& web_root, std::uint16_t port);
    void start();
    void stop();
};

#endif // FILESERVER_HPP
