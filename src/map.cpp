#include <string>
#include <sstream>
#include <fstream>
#include <nlohmann/json.hpp>
using json = nlohmann::json;

#include "map.hpp"
#include "util/util.hpp"

Map::Map(std::string const& path) {
    json j_map;
    {
        std::stringstream ss;
        std::ifstream ifs (path);
        ss << ifs.rdbuf();
        ifs.close();
        j_map = json::parse(ss.str());
    }

    platforms.reserve(j_map["platforms"].size());
    for (auto const& platform : j_map["platforms"]) {
        std::vector<std::vector<point>> shape;
        for (auto const& s : platform["shape"]) {
            shape.emplace_back();
            for (auto const& p : s) {
                shape.back().emplace_back(p["x"], p["y"]);
            }
        }

        platforms.emplace_back(Platform{shape, platform["material"]});
    }

    gravity = point(j_map["gravity"]["x"], j_map["gravity"]["y"]);

    width               = j_map["width"];
    height              = j_map["height"];
    y_limit             = j_map["y_limit"];
    respawn_x_limits[0] = j_map["respawn_x_limits"]["start"];
    respawn_x_limits[1] = j_map["respawn_x_limits"]["end"];
    respawn_height      = j_map["respawn_height"];
}

Map::Map(Map const& m)
    : platforms(m.platforms), width(m.width), height(m.height),
      y_limit(m.y_limit), respawn_x_limits(m.respawn_x_limits),
      respawn_height(m.respawn_height), gravity(m.gravity)
{}

bool Map::inside(point const& p, const bool add_padding) const {
    if (add_padding)
        return p.x > -0.5f && p.y > -0.5f &&
               p.x < width + 0.5f &&
               p.y < y_limit + 0.5f;
    else
        return p.x > 0.f && p.y > 0.f &&
               p.x < width &&
               p.y < y_limit;
}

const std::vector<float> material_frictions = {0.85f, 0.06f};
const std::vector<float> material_restitutions = {0.2f, 0.1f};

b2Body* Map::makeBox2DGround(b2World& world) const {
    b2BodyDef groundBodyDef;
    groundBodyDef.type = b2_staticBody;
    b2Body* groundBody = world.CreateBody(&groundBodyDef);

    for (auto const& platform:platforms) {
        for (auto const& point_list:platform.shape) {
            b2FixtureDef fixture_def;
            auto const b2points = Util::map<point, b2Vec2>([](point const& p){return p.b2d();}, point_list);
            b2PolygonShape polygon;
            polygon.Set(&b2points[0], b2points.size());
            fixture_def.shape = &polygon;
            fixture_def.friction = material_frictions[platform.material];
            fixture_def.restitution = material_restitutions[platform.material];
            groundBody->CreateFixture(&fixture_def);
        }
    }

    return groundBody;
}
