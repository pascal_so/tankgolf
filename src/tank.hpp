#ifndef TANK_HPP
#define TANK_HPP

#include "box2d_polyfill.hpp"
#include <Box2D/Box2D.h>
#define _USE_MATH_DEFINES
#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323
#endif

#include "types/game_state_types.hpp"
#include "types/protocol_object_types.hpp"
#include "types/point.hpp"
#include "util/util.hpp"

/*

  Box2D uses metres as length units, and is optimized for moving objects of size 0.1m to 10m.

  Tanks are therefore now by convention about 1m long.

 */

class Tank {
    b2Body* body;

    // relative turret point of rotation from tank origin
    const b2Vec2 turret_origin = {0, 0.7};

    bool active = false;

public:
    Tank(b2World &world) {
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.position = {-1.0, -1.0};
        bodyDef.angle = 0.0;
        bodyDef.allowSleep = false;
        body = world.CreateBody(&bodyDef);

        std::vector<b2Vec2> tank_polygon {{-0.55, 0.0},
                                          { 0.55, 0.0},
                                          { 0.70, 0.5},
                                          { 0.30, 0.9},
                                          {-0.30, 0.9},
                                          {-0.70, 0.5}};
        b2PolygonShape tankShape;
        tankShape.Set(&tank_polygon[0], tank_polygon.size());

        b2FixtureDef fixtureDef;
        fixtureDef.shape = &tankShape;
        fixtureDef.restitution = 0.2; // bounciness
        fixtureDef.friction = 0.3f;
        fixtureDef.density = 1.0f;

        body->CreateFixture(&fixtureDef);
        body->SetActive(false);
    }

    point getVelocity() const { return body->GetLinearVelocity(); }

    float getAngularVelocity() const { return body->GetAngularVelocity(); }

    point getPosition() const { return body->GetPosition(); }

    float getRotation() const {
        const float tau = M_PI * 2;
        return fmod(fmod(body->GetAngle(), tau) + tau, tau);
    }

    void setTransform(point const& position, float rotation) {
        if (position != point {-1, -1}) {
            body->SetActive(true);
            active = true;
        }
        body->SetTransform(position.b2d(), rotation);
    }

    point getTurretOrigin() const { return body->GetWorldPoint(turret_origin); }

    Orientation getOrientation() const { return {this->getPosition(), this->getRotation()}; }

    void setOrientation(Orientation const& o) { setTransform(o.pos, o.rot); }

    void stopMoving() {
        body->SetLinearVelocity({0, 0});
        body->SetAngularVelocity(0);
    }

    // returns {pos, vel}
    std::pair<point, point> shootBullet(ShootCommit const& shoot_command) const {
        float angle = shoot_command.angle;
        float intensity = shoot_command.intensity;

        const float epsillon = 0.0001;

        {
            const bool angle_ok = -M_PI / 2 - epsillon <= angle && angle <= M_PI / 2 + epsillon;
            const bool intensity_ok = 0 - epsillon <= intensity && intensity <= 1 + epsillon;

            if (!angle_ok) {
                std::cerr << "Warning: Invalid shooting angle: " << angle << '\n';
                angle = Util::clamp<float>(angle, -M_PI / 2, M_PI / 2);
            }
            if (!intensity_ok) {
                std::cerr << "Warning: Invalid shooting intensity: " << intensity << '\n';
                intensity = Util::clamp<float>(intensity, 0, 1);
            }
        }

        const float globalAngle = this->getRotation() + angle;

        const point direction (-std::sin(globalAngle), std::cos(globalAngle));

        const float turretLength = 0.7;
        const float finalIntensity = intensity * 15; // global intensity scaling

        return {this->getTurretOrigin() + direction * turretLength,
                direction * finalIntensity};
    }

    void deactivate() {
        this->setTransform({-1, -1}, 0);
        body->SetActive(false);
        active = false;
    }

    bool isActive() const {
        return active;
    }

    void applyForce(point const& origin) {
        // Offset the point where the force is applied to get gooder trajectories(TM)
        const b2Vec2 center_of_mass = body->GetWorldCenter()  + b2Vec2{0, 0.15};
        b2Vec2 force = center_of_mass - origin.b2d();
        force *= 300.0 / force.LengthSquared();
        body->ApplyForce(force, center_of_mass, true);
    }

    bool touches(const b2Body *const other) const {
        for (b2ContactEdge* ce = body->GetContactList(); ce; ce = ce->next) {
            if (!ce->contact->IsTouching())
                continue;
            b2Body* bodyA = ce->contact->GetFixtureA()->GetBody();
            b2Body* bodyB = ce->contact->GetFixtureB()->GetBody();

            if (other == bodyA || other == bodyB) {
                return true;
            }
        }
        return false;
    }

    // When called with itself as `other`, this returns true iff the tank is
    // touching anything.
    bool touches(Tank const& other) const {
        return this->touches(other.body);
    }
};


#endif // TANK_HPP
