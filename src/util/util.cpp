#include "util.hpp"

namespace Util {

json vectorToJsonArray(std::vector<json> const& vec) {
    json j = json::array();
    for (auto const& v:vec) {
        j.push_back(v);
    }
    return j;
}

json readJson(std::istream& stream) {
    json j;
    stream >> j;
    return j;
}

}
