#ifndef UTIL_HPP
#define UTIL_HPP

#include <vector>
#include <iostream>
#include <nlohmann/json.hpp>
using json = nlohmann::json;

namespace Util{

template<class T, class Compare>
constexpr const T& clamp( const T& v, const T& lo, const T& hi, Compare comp )
{
    return assert( !comp(hi, lo) ),
        comp(v, lo) ? lo : comp(hi, v) ? hi : v;
}

template<class T>
constexpr const T& clamp( const T& v, const T& lo, const T& hi )
{
    // hack to make it still compile with c++17.
    namespace _ns = ::Util;
    return _ns::clamp( v, lo, hi, std::less<>() );
}

json vectorToJsonArray(std::vector<json> const& vec);

json readJson(std::istream& stream);

template<typename T, typename T2, typename Func>
std::vector<T2> map(Func f, std::vector<T> const& in) {
    std::vector<T2> out;
    out.reserve(in.size());
    for (auto const& i:in) {
        out.push_back(f(i));
    }
    return out;
}

template<typename T>
void append(std::vector<T>& base, std::vector<T> const& rest) {
    base.insert(base.end(), rest.begin(), rest.end());
}

} // namespace Util

#endif // UTIL_HPP
