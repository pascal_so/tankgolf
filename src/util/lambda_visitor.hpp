#ifndef LAMBDA_VISITOR_HPP
#define LAMBDA_VISITOR_HPP

#include <boost/variant.hpp>

// Hack: https://stackoverflow.com/a/7870614

template <typename ReturnType, typename... Lambdas> struct lambda_visitor;

template <typename ReturnType, typename Lambda1, typename... Lambdas>
struct lambda_visitor<ReturnType, Lambda1, Lambdas...>
    : Lambda1, lambda_visitor<ReturnType, Lambdas...> {

    using Lambda1::operator();
    using lambda_visitor<ReturnType, Lambdas...>::operator();
    lambda_visitor(Lambda1 l1, Lambdas... lambdas)
        : Lambda1(l1), lambda_visitor<ReturnType, Lambdas...>(lambdas...) {}
};

template <typename ReturnType, typename Lambda1>
struct lambda_visitor<ReturnType, Lambda1> : Lambda1,
                                             boost::static_visitor<ReturnType> {

    using Lambda1::operator();
    lambda_visitor(Lambda1 l1)
        : Lambda1(l1), boost::static_visitor<ReturnType>() {}
};

template <typename ReturnType>
struct lambda_visitor<ReturnType> : boost::static_visitor<ReturnType> {

    lambda_visitor() : boost::static_visitor<ReturnType>() {}
};

template <typename ReturnType, typename... Lambdas>
lambda_visitor<ReturnType, Lambdas...> make_lambda_visitor(Lambdas... lambdas) {
    return {lambdas...};
}

template <typename ReturnType, typename VariantType, typename... Lambdas>
ReturnType apply_lambda_visitor(VariantType const& var, Lambdas... lambdas){
    return boost::apply_visitor(make_lambda_visitor<ReturnType>(lambdas...), var);
}

#endif // LAMBDA_VISITOR_HPP
