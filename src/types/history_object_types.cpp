#include "history_object_types.hpp"
#include "../util/lambda_visitor.hpp"
#include "../util/util.hpp"

namespace History {

HistoryObject::HistoryObject(Event const& event, std::uint8_t active_player_id, bool simulated)
    : event(event), active_player_id(active_player_id), simulated(simulated) {}

std::array<unsigned, 2> getScores(std::vector<HistoryObject> const& hist, bool count_simulated) {
    std::array<unsigned, 2> scores {0, 0};
    for (auto const& h:hist) {
        if(h.simulated && !count_simulated) continue;
        apply_lambda_visitor<void>(
            h.event,
            [&scores](FallOffMapEvent const& fall_event) {
                scores[fall_event.player_id ^ 1u] ++;
            },
            [](auto const&) {}
        );
    }
    return scores;
}

void HistoryObject::flip() {
    active_player_id ^= 1u;
    event = apply_lambda_visitor<Event>(
        event,
        [](FallOffMapEvent fall_event) {
            fall_event.player_id ^= 1u;
            return fall_event;
        },
        [](Motion const& m) {
            return Util::map<PhysicsState, PhysicsState>([](auto s){s.flip(); return s;}, m);
        },
        [](auto const& o) {
            return o;
        }
    );
}

json jsonMotion(Motion const& objs) {
    return Util::vectorToJsonArray(Util::map<PhysicsState, json>(jsonPhysicsState, objs));
}

json jsonEvent(Event const& ev) {
    json j;

    apply_lambda_visitor<void>(
        ev,
        [&j](RespawnEvent const& o) {
            j["type"] = "respawn";
            j["x_position"] = o.x_position;
        },
        [&j](ShootEvent const& o) {
            j["type"] = "shoot";
            j["angle"] = o.angle;
            j["intensity"] = o.intensity;
        },
        [&j](FallOffMapEvent const& o) {
            j["type"] = "fall_off_map";
            j["player_id"] = o.player_id;
        },
        [&j](BulletImpactEvent const& o) {
            j["type"] = "impact";
            j["pos"] = {o.pos.x, o.pos.y};
        },
        [&j](Motion const& m) {
            j["type"] = "motion";
            j["trajectory"] = jsonMotion(m);
        }
    );
    return j;
}

json jsonHistoryObject(HistoryObject const& obj) {
    json j;
    j["event"] = jsonEvent(obj.event);
    j["active_player_id"] = obj.active_player_id;
    j["simulated"] = obj.simulated;
    return j;
}

void flipPlayers(std::vector<HistoryObject>& h) {
    h = Util::map<HistoryObject, HistoryObject>([](auto h){h.flip(); return h;}, h);
}

bool is_motion(const HistoryObject& ho) {
    return apply_lambda_visitor<bool>(
        ho.event,
        [](Motion const&) { return true; },
        [](auto const&) { return false; }
    );
}

} // namespace History
