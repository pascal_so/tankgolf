#ifndef GAME_STATE_TYPES_HPP
#define GAME_STATE_TYPES_HPP

#include <array>
#include <boost/variant.hpp>
#include <iostream>
#include <nlohmann/json.hpp>
#define _USE_MATH_DEFINES
#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323
#endif

#include "point.hpp"

using json = nlohmann::json;

struct Orientation {
    point pos {-1, -1};
    float rot {0};

    Orientation() = default;
    Orientation(const point& p, const float r);

    bool operator==(Orientation const& o) const;
};

using TankOrientations = std::array<Orientation, 2>;

void flipPlayers(TankOrientations& state);

struct PhysicsState {
    TankOrientations tanks;
    point bullet {-1, -1};

    PhysicsState() = default;
    PhysicsState(TankOrientations const& tanks, point const& bullet = {-1, -1});

    bool operator==(PhysicsState const& o) const;

    void flip();
};

struct GameState {
    PhysicsState physics_state;
    std::array<unsigned, 2> scores {{0, 0}};

    GameState() = default;
    GameState(PhysicsState const& physics_state, std::array<unsigned, 2> scores);

    bool operator==(GameState const& o) const;

    void flip();
};

template<typename T>
void flipPlayers(T& state) {
    state.flip();
}

GameState parseStringGameState(std::istream& stream);
PhysicsState parseStringPhysicsState(std::istream& stream);
TankOrientations parseStringTankOrientations(std::istream& stream);

GameState parseJsonGameState(json const& j);
PhysicsState parseJsonPhysicsState(json const& j);
TankOrientations parseJsonTankOrientations(json const& j);

// This uses the string protocol, not JSON. To serialize to JSON format,
// use the functions below that return a nlohmann::json object.
std::ostream& operator<<(std::ostream& os, GameState const& state);
std::ostream& operator<<(std::ostream& os, PhysicsState const& state);
std::ostream& operator<<(std::ostream& os, TankOrientations const& state);

json jsonGameState(GameState const& state);
json jsonPhysicsState(PhysicsState const& state);
json jsonTankOrientations(TankOrientations const& state);

enum class RoundType {Respawn, Shoot};

namespace PlayerTypes {
struct Websocket {};

struct Bot {
    std::string command;
};

struct Server {
    std::string url;
};
}

using PlayerType = boost::variant<PlayerTypes::Websocket, PlayerTypes::Bot,
                                  PlayerTypes::Server>;


#endif // GAME_STATE_TYPES_HPP
