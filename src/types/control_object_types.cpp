#include "control_object_types.hpp"
#include "../util/lambda_visitor.hpp"
#include "../util/util.hpp"

#include <nlohmann/json.hpp>
using json = nlohmann::json;

ControlObject parseJsonControlObject(std::istream& is) {
    json j;
    is >> j;
    std::string type = j["type"];

    if (type == "player_descriptions") {
        std::array<PlayerType, 2> players;

        for (std::size_t i = 0; i < 2; ++i) {
            auto const& p = j["players"][i];
            if (p["type"] == "websocket") {
                players[i] = PlayerTypes::Websocket();
            } else if (p["type"] == "bot") {
                players[i] = PlayerTypes::Bot{ p["argument"].get<std::string>() };
            } else if (p["type"] == "server") {
                players[i] = PlayerTypes::Server{ p["argument"].get<std::string>() };
            } else {
                throw std::invalid_argument("Unknown player type: '" +
                    p["type"].dump() + "'.");
            }
        }

        return ControlObjectTypes::PlayerList{ players };
    } else if (type == "stop_game") {
        return ControlObjectTypes::StopGame();
    } else if (type == "stop_server") {
        return ControlObjectTypes::StopServer();
    } else if (type == "finished_playback") {
        return ControlObjectTypes::FinishedPlayback();
    } else {
        throw std::invalid_argument("Invalid type for control object found "
            "while parsing from JSON: '" + type + "'.");
    }
}

std::string formatJsonControlObject(ControlObject const& obj) {
    json j;
    apply_lambda_visitor<void>(
        obj,
        [&j](ControlObjectTypes::StartRound const& o) {
            j["type"] = "start_round";
            j["round_type"] = (o.round_type == RoundType::Shoot ? "shoot" : "respawn");
            j["player_id"] = o.player_id;
            j["state"] = jsonGameState(o.state);
        },
        [&j](ControlObjectTypes::EndRound const&) {
            j["type"] = "end_round";
        },
        [&j](std::vector<History::HistoryObject> const& o) {
            j["type"] = "history";
            j["history"] = Util::vectorToJsonArray(Util::map<History::HistoryObject, json>(
                               History::jsonHistoryObject, o));
        },
        [&](auto const&) {
            // The other control objects will only ever be received, never sent.
            // If you want to extend the formatter to handle them anyway, feel free.
            throw std::invalid_argument("Received unsupported type control "
                "object while serializing to JSON.");
        }
    );

    return j.dump();
}
