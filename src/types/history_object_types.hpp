#ifndef HISTORY_HPP
#define HISTORY_HPP

#include <vector>
#include <fstream>
#include <nlohmann/json.hpp>

#include "point.hpp"
#include "game_state_types.hpp"

using json = nlohmann::json;

namespace History{

struct RespawnEvent {
    float x_position;
};
struct ShootEvent {
    float angle;
    float intensity;
};
struct FallOffMapEvent {
    // point position;
    // We don't care about the position, have a
    // look at the trajetory if you really need it.

    std::uint8_t player_id;
};
struct BulletImpactEvent {
    point pos;
};

using Motion = std::vector<PhysicsState>;

using Event = boost::variant<RespawnEvent,
                             ShootEvent,
                             FallOffMapEvent,
                             BulletImpactEvent,
                             Motion>;

struct HistoryObject {
    Event event;

    std::uint8_t active_player_id;
    bool simulated;

    HistoryObject(Event const& event, std::uint8_t active_player_id, bool simulated);

    void flip();
};

bool is_motion(const HistoryObject& ho);

std::array<unsigned, 2> getScores(std::vector<HistoryObject> const& hist, bool count_simulated = false);

json jsonMotion(Motion const& objs);
json jsonEvent(Event const& ev);
json jsonHistoryObject(HistoryObject const& obj);

void flipPlayers(std::vector<HistoryObject>& h);

} // namespace History


#endif // HISTORY_HPP
