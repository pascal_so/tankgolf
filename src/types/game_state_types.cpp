#include "game_state_types.hpp"

Orientation::Orientation(const point& p, const float r) : pos(p), rot(r) {
    rot = fmod(rot, 2 * M_PI);
}

bool Orientation::operator==(Orientation const& o) const {
    return pos == o.pos && 
           rot == o.rot;
}

void flipPlayers(TankOrientations& state) {
    std::swap(state[0], state[1]);
}

PhysicsState::PhysicsState(TankOrientations const& tanks, point const& bullet)
    : tanks(tanks), bullet(bullet) {}

bool PhysicsState::operator==(PhysicsState const& o) const {
    return tanks == o.tanks &&
           bullet == o.bullet;
}

void PhysicsState::flip() {
    flipPlayers(tanks);
}

GameState::GameState(PhysicsState const& physics_state, std::array<unsigned, 2> scores) 
    : physics_state(physics_state), scores(scores) {}

bool GameState::operator==(GameState const& o) const {
    return physics_state == o.physics_state && 
           scores == o.scores;
}

void GameState::flip() {
    flipPlayers(physics_state);
    std::swap(scores[0], scores[1]);
}

GameState parseStringGameState(std::istream& stream) {
    GameState state;

    state.physics_state = parseStringPhysicsState(stream);

    unsigned scoreA, scoreB;
    stream >> scoreA >> scoreB;
    state.scores = {scoreA, scoreB};

    if (stream.fail()) {
        throw std::invalid_argument("Could not parse game state.");
    }

    return state;
}

PhysicsState parseStringPhysicsState(std::istream& stream) {
    PhysicsState state;

    state.tanks = parseStringTankOrientations(stream);

    float bullet_x, bullet_y;
    stream >> bullet_x >> bullet_y;
    state.bullet = {bullet_x, bullet_y};

    if (stream.fail()) {
        throw std::invalid_argument("Could not parse physics state.");
    }

    return state;
}

TankOrientations parseStringTankOrientations(std::istream& stream) {
    TankOrientations tank_orientations;

    float x, y, r;
    for (auto i : {0, 1}) {
        stream >> x >> y >> r;
        point pos = {x, y};
        float rot = fmod(r, 2 * M_PI);
        tank_orientations[i] = Orientation{pos, rot};
    }

    if (stream.fail()) {
        throw std::invalid_argument("Could not parse tank orientations.");
    }

    return tank_orientations;
}

GameState parseJsonGameState(json const& j) {
    auto const physics_state = parseJsonPhysicsState(j["physics_state"]);
    std::array<unsigned, 2> const scores = {j["scores"][0], j["scores"][1]};

    return GameState(physics_state, scores);
}

PhysicsState parseJsonPhysicsState(json const& j) {
    auto const tanks = parseJsonTankOrientations(j);
    point const bullet = {j["bullet"][0], j["bullet"][1]};

    return PhysicsState(tanks, bullet);
}

TankOrientations parseJsonTankOrientations(json const& j) {
    TankOrientations tank_orientations;

    for (std::size_t i = 0; i < j["tanks"].size(); ++i) {
        auto &tank = j["tanks"][i];    
        float x = tank["pos"][0];
        float y = tank["pos"][1];
        point pos = {x, y};
        float rot = fmod(tank["rot"], 2 * M_PI);
        tank_orientations[i] = Orientation{pos, rot};
    }

    return tank_orientations;
}

std::ostream& operator<<(std::ostream& os, GameState const& state) {
    return os << state.physics_state << ' ' << state.scores[0] << ' ' << state.scores[1];
}

std::ostream& operator<<(std::ostream& os, PhysicsState const& state) {
    return os << state.tanks << ' ' << state.bullet.x << ' ' << state.bullet.y;
}

std::ostream& operator<<(std::ostream& os, TankOrientations const& tank_orientations) {
    for (std::size_t i = 0; i < tank_orientations.size(); ++i) {
        auto& tank = tank_orientations[i];
        os << tank.pos.x << ' ' << tank.pos.y << ' ' << tank.rot;

        if (i != tank_orientations.size() - 1) {
            os << ' ';
        }
    }

    return os;
}

json jsonGameState(GameState const& state) {
    json j;
    j["physics_state"] = jsonPhysicsState(state.physics_state);
    j["scores"] = {state.scores[0], state.scores[1]};
    return j;
}

json jsonPhysicsState(PhysicsState const& state) {
    json j = jsonTankOrientations(state.tanks);
    j["bullet"] = {state.bullet.x, state.bullet.y};
    return j;
}

json jsonTankOrientations(TankOrientations const& tank_orientations) {
    json j;
    j["tanks"] = json::array();
    for (auto const& tank : tank_orientations) {
        j["tanks"].push_back({
            {"pos", {tank.pos.x, tank.pos.y}},
            {"rot", tank.rot}
        });
    }
    return j;
}

