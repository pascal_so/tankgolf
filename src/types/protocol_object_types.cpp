#include "protocol_object_types.hpp"
#include "../util/lambda_visitor.hpp"
#include "game_state_types.hpp"

#include <sstream>

#include <nlohmann/json.hpp>
using json = nlohmann::json;

std::string formatStringProtocol(ProtocolObject const& obj) {
    std::stringstream ss;
    apply_lambda_visitor<void>(
        obj,
        [&ss](const ShootRoundStart& obj) {
            ss << "startshoot " << obj.state;
        },
        [&ss](const RespawnRoundStart& obj) {
            ss << "startrespawn " << obj.state;
        },
        [&ss](const QueryShoot& obj) {
            ss << "queryshoot " << obj.angle << ' ' << obj.intensity;
        },
        [&ss](const QueryRespawn& obj) {
            ss << "queryrespawn " << obj.x_position;
        },
        [&ss](const FullQueryShoot& obj) {
            ss << "fullqueryshoot " << obj.state;
        },
        [&ss](const FullQueryRespawn& obj) {
            ss << "fullqueryrespawn " << obj.state;
        },
        [&ss](const GameState& game_state) {
            ss << "state " << game_state;
        },
        [&ss](const ShootCommit& obj) {
            ss << "shoot " << obj.angle << ' ' << obj.intensity;
        },
        [&ss](const RespawnCommit& obj) {
            ss << "respawn " << obj.x_position;
        },
        [&ss](const GameOverMessage&) {
            ss << "gameover";
        }
    );

    return ss.str();
}

std::string formatJsonProtocol(ProtocolObject const& obj) {
    json j;
    apply_lambda_visitor<void>(
        obj,
        [&j](const ShootRoundStart& obj) {
            j["type"] = "startshoot";
            j["state"] = jsonGameState(obj.state);
        },
        [&j](const RespawnRoundStart& obj) {
            j["type"] = "startrespawn";
            j["state"] = jsonGameState(obj.state);
        },
        [&j](const QueryShoot& obj) {
            j["type"] = "queryshoot";
            j["angle"] = obj.angle;
            j["intensity"] = obj.intensity;
        },
        [&j](const QueryRespawn& obj) {
            j["type"] = "queryrespawn";
            j["x_position"] = obj.x_position;
        },
        [&j](const FullQueryShoot& obj) {
            j["type"] = "fullqueryshoot";
            j["tank_orientations"] = jsonTankOrientations(obj.state);
        },
        [&j](const FullQueryRespawn& obj) {
            j["type"] = "fullqueryrespawn";
            j["tank_orientations"] = jsonTankOrientations(obj.state);
        },
        [&j](const GameState& game_state) {
            j["type"] = "state";
            j["state"] = jsonGameState(game_state);
        },
        [&j](const ShootCommit& obj) {
            j["type"] = "shoot";
            j["angle"] = obj.angle;
            j["intensity"] = obj.intensity;
        },
        [&j](const RespawnCommit& obj) {
            j["type"] = "respawn";
            j["x_position"] = obj.x_position;
        },
        [&j](const GameOverMessage&) {
            j["type"] = "gameover";
        }
    );

    return j.dump();
}

ProtocolObject parseStringProtocolObject(std::istream& stream) {
    std::string query_type;

    stream >> query_type;

    if (query_type == "startshoot") {
        return ShootRoundStart{parseStringGameState(stream)};
    } else if (query_type == "startrespawn") {
        return RespawnRoundStart{parseStringGameState(stream)};
    } else if (query_type == "queryshoot") {
        float angle, intensity;
        stream >> angle >> intensity;
        return QueryShoot{angle, intensity};
    } else if (query_type == "queryrespawn") {
        float x_position;
        stream >> x_position;
        return QueryRespawn{x_position};
    } else if (query_type == "fullqueryshoot") {
        auto state = parseStringTankOrientations(stream);
        float angle, intensity;
        stream >> angle >> intensity;
        return FullQueryShoot{state, angle, intensity};
    } else if (query_type == "fullqueryrespawn") {
        auto state = parseStringTankOrientations(stream);
        float x_position;
        stream >> x_position;
        return FullQueryRespawn{state, x_position};
    } else if (query_type == "state") {
        return parseStringGameState(stream);
    } else if (query_type == "shoot") {
        float angle, intensity;
        stream >> angle >> intensity;
        return ShootCommit{angle, intensity};
    } else if (query_type == "respawn") {
        float x_position;
        stream >> x_position;
        return RespawnCommit{x_position};
    } else {
        throw std::invalid_argument("Unknown type for string protocol: '" + query_type + "'.");
    }
}

ProtocolObject parseJsonProtocolObject(json const& j) {
    std::string query_type = j["type"];

    if (query_type == "startshoot") {
        return ShootRoundStart{parseJsonGameState(j["state"])};
    } else if (query_type == "startrespawn") {
        return ShootRoundStart{parseJsonGameState(j["state"])};
    } else if (query_type == "queryshoot") {
        return QueryShoot{j["angle"], j["intensity"]};
    } else if (query_type == "queryrespawn") {
        return QueryRespawn{j["x_position"]};
    } else if (query_type == "fullqueryshoot") {
        const auto state = parseJsonTankOrientations(j["tank_orientations"]);
        return FullQueryShoot{state, j["angle"], j["intensity"]};
    } else if (query_type == "fullqueryrespawn") {
        const auto state = parseJsonTankOrientations(j["tank_orientations"]);
        return FullQueryRespawn{state, j["x_position"]};
    } else if (query_type == "state") {
        return parseJsonGameState(j["state"]);
    } else if (query_type == "shoot") {
        return ShootCommit{j["angle"], j["intensity"]};
    } else if (query_type == "respawn") {
        return RespawnCommit{j["x_position"]};
    } else {
        throw std::invalid_argument("Unknown type for json protocol: '" + query_type + "'.");
    }
}

bool isQuery(ProtocolObject const& po) {
    return apply_lambda_visitor<bool>(
        po,
        [](QueryShoot const&) { return true; },
        [](QueryRespawn const&) { return true; },
        [](FullQueryShoot const&) { return true; },
        [](FullQueryRespawn const&) { return true; },
        [](auto) { return false; }
    );
}

std::string strProtocolObjectName(ProtocolObject const& obj) {
    return apply_lambda_visitor<std::string>(
        obj,
        [](RespawnRoundStart const&){ return "RespawnRoundStart"; },
        [](ShootRoundStart const&){ return "ShootRoundStart"; },
        [](QueryShoot const&){ return "QueryShoot"; },
        [](QueryRespawn const&){ return "QueryRespawn"; },
        [](FullQueryShoot const&){ return "FullQueryShoot"; },
        [](FullQueryRespawn const&){ return "FullQueryRespawn"; },
        [](GameState const&){ return "GameState"; },
        [](ShootCommit const&){ return "ShootCommit"; },
        [](RespawnCommit const&){ return "RespawnCommit"; },
        [](GameOverMessage const&){ return "GameOverMessage"; }
        );
}
