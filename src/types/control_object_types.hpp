#ifndef CONTROL_OBJECT_TYPES_HPP
#define CONTROL_OBJECT_TYPES_HPP

#include <boost/variant.hpp>
#include <string>

#include "history_object_types.hpp"
#include "game_state_types.hpp"

namespace ControlObjectTypes {
//// Browser to Server ///////

struct PlayerList {
    std::array<PlayerType, 2> players;
};

struct StopGame {};

struct StopServer {};

struct FinishedPlayback {};

//// Server to Browser ///////

struct StartRound {
    RoundType round_type;
    std::uint8_t player_id;
    GameState state;
};

struct EndRound {};
}

using ControlObject = boost::variant<ControlObjectTypes::PlayerList,
                                     ControlObjectTypes::StopGame,
                                     ControlObjectTypes::StopServer,
                                     ControlObjectTypes::FinishedPlayback,
                                     ControlObjectTypes::StartRound,
                                     ControlObjectTypes::EndRound,
                                     std::vector<History::HistoryObject>>;

ControlObject parseJsonControlObject(std::istream& is);
std::string formatJsonControlObject(ControlObject const& obj);

#endif // CONTROL_OBJECT_TYPES_HPP
