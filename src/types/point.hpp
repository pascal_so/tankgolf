#ifndef POINT_HPP
#define POINT_HPP

#include "../box2d_polyfill.hpp"
#include <Box2D/Common/b2Math.h>
#include <iostream>
#include <ostream>

struct point {
    float x;
    float y;

    point() : x(0), y(0) {}

    point(float x, float y) : x(x), y(y) {}

    point(const point& o) : x(o.x), y(o.y) {}

    point(const b2Vec2& p) : x(p.x), y(p.y) {}

    b2Vec2 b2d() const { return {x, y}; }

    point operator+(const point& o) const { return point(x + o.x, y + o.y); }

    point operator-(const point& o) const { return point(x - o.x, y - o.y); }

    inline friend point operator*(const point& p, float f) {
        return point(p.x * f, p.y * f);
    }

    inline friend point operator*(float f, const point& p) {
        return point(p.x * f, p.y * f);
    }

    point& operator+=(const point& o) {
        x += o.x;
        y += o.y;

        return *this;
    }

    point& operator-=(const point& o) {
        x -= o.x;
        y -= o.y;

        return *this;
    }

    point& operator*=(float f) {
        x *= f;
        y *= f;
        return *this;
    }

    bool operator<(const point& o) const {
        return std::tie(x, y) < std::tie(o.x, o.y);
    }

    bool operator==(const point& o) const { return x == o.x && y == o.y; }

    bool operator!=(const point& o) const { return !((*this) == o); }
};

inline std::ostream& operator<<(std::ostream& out, const point& p) {
    return out << "(" << p.x << ", " << p.y << ")";
}

#endif // POINT_HPP
