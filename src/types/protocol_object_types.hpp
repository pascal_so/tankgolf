#ifndef PROTOCOL_OBJECT_TYPES_HPP
#define PROTOCOL_OBJECT_TYPES_HPP

#include <boost/variant.hpp>

#include "game_state_types.hpp"

struct RespawnRoundStart {
    GameState state;
};
struct ShootRoundStart {
    GameState state;
};
struct QueryShoot {
    float angle;
    float intensity;
};
struct QueryRespawn {
    float x_position;
};
struct FullQueryShoot {
    TankOrientations state;
    float angle;
    float intensity;
};
struct FullQueryRespawn {
    TankOrientations state;
    float x_position;
};
struct ShootCommit {
    float angle;
    float intensity;
};
struct RespawnCommit {
    float x_position;
};
struct GameOverMessage {
};

using ProtocolObject =
    boost::variant<GameState, ShootRoundStart, RespawnRoundStart, QueryShoot,
                   QueryRespawn, FullQueryShoot, FullQueryRespawn, ShootCommit,
                   RespawnCommit, GameOverMessage>;

using ProtocolBackend = std::function<std::string(ProtocolObject)>;

bool isQuery(ProtocolObject const& po);

ProtocolObject parseStringProtocolObject(std::istream& stream);
ProtocolObject parseJsonProtocolObject(json const& j);

std::string formatStringProtocol(ProtocolObject const& obj);
std::string formatJsonProtocol(ProtocolObject const& obj);

std::string strProtocolObjectName(ProtocolObject const& obj);

#endif // PROTOCOL_OBJECT_TYPES_HPP
