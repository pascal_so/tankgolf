#include "logger.hpp"

#include "types/control_object_types.hpp"
#include "util/util.hpp"

void Logger::log(std::vector<History::HistoryObject> objs) {
    if (flipped) {
        flipPlayers(objs);
    }

    log_internal(objs);
}

void Logger::close() {}

FileLogger::FileLogger(std::string const& filename) {
    file.open(filename);
}

void FileLogger::close() {
    file.close();
}

void FileLogger::log_internal(std::vector<History::HistoryObject> const& objs) {
    // Write events and motions each as individual line of json, not contained
    // in a big array.
    for (auto const& obj:objs) {
        file << jsonHistoryObject(obj).dump() << '\n';
    }
}

NetworkLogger::NetworkLogger(std::shared_ptr<blocking_queue<OutgoingMessage>> queue,
              connection_hdl const& hdl)
    : queue(queue), hdl(hdl) {}

void NetworkLogger::log_internal(std::vector<History::HistoryObject> const& objs) {
    std::string msg = formatJsonControlObject(objs);
    queue->push(OutgoingMessage{hdl, msg});
}

CombinedLogger::CombinedLogger(std::vector<std::unique_ptr<Logger>> backends) : backends(std::move(backends)) {}

void CombinedLogger::log_internal(std::vector<History::HistoryObject> const& objs) {
    for (auto& b:backends) {
        b->log(objs);
    }
}

void CombinedLogger::close() {
    for (auto& b:backends) {
        b->close();
    }
}
