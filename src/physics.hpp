#ifndef PHYSICS_HPP
#define PHYSICS_HPP

#include <utility>
#include <vector>

#include "map.hpp"
#include "tank.hpp"
#include "types/game_state_types.hpp"
#include "types/history_object_types.hpp"
#include "types/point.hpp"
#include "util/lambda_visitor.hpp"

class PhysicsEngine {
    Map map;
    b2World world;
    b2Body* ground;
    std::array<Tank, 2> tanks;
    point bullet_pos;
    point bullet_vel;
    point impact_pos;

    const float32 timeStep = 1.0f / 30.0f;

    bool tickPhysics();

public:
    PhysicsEngine(Map const& map);

    void setFromState(GameState const& state);
    void setFromState(PhysicsState const& state);
    void setFromState(TankOrientations const& tank_orientations);

    PhysicsState getPhysicsState() const;
    PhysicsState getPhysicsStateImpact() const;
    TankOrientations getTankOrientations() const;

    std::pair<std::vector<point>, bool> shoot(std::uint8_t player_id, ShootCommit const& shoot_command);
    void explodeBullet();
    std::vector<TankOrientations> moveTanks();

    std::vector<History::HistoryObject>
    handleAction(TankOrientations state,
                  std::uint8_t player_id,
                  bool simulated,
                  boost::variant<ShootCommit, RespawnCommit> const& action);
};

#endif // PHYSICS_HPP

