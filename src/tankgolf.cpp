#include <iostream>
#include <thread>

#include "server.hpp"
#include "fileserver/fileserver.hpp"

using namespace std::chrono_literals;

int main() {
    GameServer game_server;

    // WARNING: this assumes that the binary is executed from the build directory.
    const std::string file_server_root = "../src/webclient/dist";
    FileServer file_server(file_server_root, 8000);

    std::thread cin_thread([&] {
        std::cout << "Starting TankGolf server. You can stop the server by "
                     "pressing CTRL-D on linux systems or CTRL-Z on Windows."
                  << std::endl;

        std::string command;
        while (std::cin >> command) {
            // maybe eventually add commands here.
        }

        game_server.stop();
    });

    std::thread file_server_thread([&file_server] {
        file_server.start();
    });

    std::this_thread::sleep_for(200ms);
    game_server.run(9002);

    file_server.stop();

    cin_thread.join();
    file_server_thread.join();
}
