#ifndef SERVER_HPP
#define SERVER_HPP

#include <vector>
#include <set>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

#include "custom_websocket_config.hpp"
#include "game.hpp"
#include "util/blocking_queue.hpp"

class GamesManager {
    std::unordered_map<int, std::unique_ptr<ServerGameHost>> active_games;

    int next_id;

    std::mutex mutex;
    using guard = std::lock_guard<std::mutex>;

public:
    GamesManager();

    int
    addGame(ws_server* server, connection_hdl control_connection_hdl,
            std::shared_ptr<blocking_queue<OutgoingMessage>> game_to_browser,
            std::shared_ptr<blocking_queue<std::string>> browser_to_game);

    void stopGame(int id);

    void stopAllGames();

    ServerGameHost* getGameById(int id);

    size_t size();
};

class GameServer {
    ws_server m_server;
    std::shared_ptr<blocking_queue<OutgoingMessage>> m_game_to_browser;

    std::atomic<bool> m_running;

    GamesManager m_games_manager;

    std::mutex m_mtx_floating_connections;
    std::set<connection_hdl, std::owner_less<connection_hdl>>
        m_floating_connections;

    bool on_validate(connection_hdl hdl);

    void on_open(connection_hdl hdl);

    void on_close(connection_hdl hdl);

    void on_message(connection_hdl hdl, ws_server::message_ptr msg);

    void process_outgoing_messages();

public:
    GameServer();

    void run(uint16_t port);

    void stop();
};

#endif // SERVER_HPP
