#include "box2d_polyfill.hpp"
#include <Box2D/Box2D.h>
#include <iostream>

#include "physics.hpp"
#include "types/game_state_types.hpp"
#include "util/util.hpp"

PhysicsEngine::PhysicsEngine(Map const& map)
    : map(map), world (map.gravity.b2d()),
      ground (map.makeBox2DGround(world)),
      tanks {Tank(world), Tank(world)}
{}

bool PhysicsEngine::tickPhysics() {
    const int32 velocityIterations = 8;
    const int32 positionIterations = 5;

    world.Step(timeStep, velocityIterations, positionIterations);
    world.ClearForces();

    for (size_t i = 0; i < tanks.size(); ++i) {
        auto& t = tanks[i];
        if (t.isActive() && !map.inside(t.getPosition())) {
#ifdef TANKGOLF_VERBOSE
            std::cerr << (i == 0 ? "Active player" : "opponent") << " fell off the map.\n";
#endif

            t.deactivate();
        }
    }

    const float vel_limit = 0.006;
    const float angular_vel_limit = 0.006;

    for (std::size_t i = 0; i < tanks.size(); ++i) {
        const auto& t = tanks[i];
        const auto& other_tank = tanks[1-i];

        if (!t.isActive())
            continue;

        const auto vel = t.getVelocity();
        const auto vsq = vel.x * vel.x + vel.y * vel.y;
        const auto angv = t.getAngularVelocity();

        if (vsq > vel_limit * vel_limit || angv > angular_vel_limit) {
            // one of the tanks is still moving
            return true;
        }

        if (!t.touches(ground) && !t.touches(other_tank)) {
            // one of the tanks is still in the air
            return true;
        }
    }

    return false;
}

// vector of bullet positions as trajectory
// bool is true when bullet hit something rather than left map
std::pair<std::vector<point>, bool> PhysicsEngine::shoot(std::uint8_t player_id, ShootCommit const& shoot_command) {

    impact_pos = {-1, -1};

    std::tie(bullet_pos, bullet_vel) = tanks[player_id].shootBullet(shoot_command);

    class BulletRayCastCallback : public b2RayCastCallback {
    public:
        b2Vec2 impact_point;
        bool hit = false;

        float32 ReportFixture(b2Fixture* /* fixture */,
                              const b2Vec2& p,
                              const b2Vec2& /* normal */,
                              float32 fraction) {
            impact_point = p;
            hit = true;
            return fraction;
        }
    } callback;

    std::vector<point> bullet_trajectory = {bullet_pos};

    bool hit = false;

    // Check if the bullet started inside an object
    for (const b2Body* body = world.GetBodyList(); body; body = body->GetNext()) {
        for (const b2Fixture* f = body->GetFixtureList(); f; f = f->GetNext()) {
            if (f->TestPoint(bullet_pos.b2d())) {
                hit = true;
                break;
            }
        }
        if (hit) break;
    }

    while (!hit && map.inside(bullet_pos)) {
        b2Vec2 point1 = bullet_pos.b2d();
        b2Vec2 point2 = (bullet_pos + bullet_vel * timeStep).b2d();

        if ((point2 - point1).LengthSquared() <= 0.0f) {
            // Fix for "Assertion failed: (r.LengthSquared() > 0.0f), function RayCast".
            // Caused by a velocity vector of zero. Can be fixed by waiting for
            // gravity to change the velocity of the bullet.
            bullet_pos = point2;
            bullet_vel += point(map.gravity) * timeStep;
            continue;
        }

        world.RayCast(&callback, point1, point2);

        if(callback.hit){
            hit = true;
            bullet_pos = callback.impact_point;
            bullet_vel = {0, 0};
            impact_pos = callback.impact_point;
        }else{
            bullet_pos = point2;
            bullet_vel += point(map.gravity) * timeStep;
        }

        bullet_trajectory.push_back(bullet_pos);
    }

    if (!hit) {
        bullet_pos = {-1, -1};
        impact_pos = {-1, -1};
    }

    return {bullet_trajectory, hit};
}

// returns n+1 states for n ticks
std::vector<TankOrientations> PhysicsEngine::moveTanks() {
    std::vector<TankOrientations> trajectories = {getTankOrientations()};

    bool moving = true;
    unsigned ticksCounter = 0;
    const unsigned ticksLimit = 30 * 100; // seconds

    while(moving) {
        moving = tickPhysics();
        trajectories.push_back(getTankOrientations());

        if (ticksCounter >= ticksLimit) {
            // Game is stuck in infinite loop and never continues -> stop physics simulation
            // Caused by getting stuck in 'complex' (eg. overlapping) geometry of the map.
#ifdef TANKGOLF_VERBOSE
            std::cerr << "Physics timeout: stopping simulation after " << ticksLimit << " ticks.\n";
#endif
            moving = false;
        }

        ++ticksCounter;
    }

    return trajectories;
}

void PhysicsEngine::setFromState(GameState const& state) {
    setFromState(state.physics_state);
}

void PhysicsEngine::setFromState(PhysicsState const& state) {
    setFromState(state.tanks);
    bullet_pos = state.bullet;
}

void PhysicsEngine::setFromState(TankOrientations const& tank_orientations) {
    for (std::size_t i = 0; i < 2; ++i) {
        if (tank_orientations[i].pos == point{-1, -1}) {
            tanks[i].deactivate();
        } else {
            tanks[i].setOrientation(tank_orientations[i]);
        }
    }
}

PhysicsState PhysicsEngine::getPhysicsState() const {
    return PhysicsState({tanks[0].getOrientation(), tanks[1].getOrientation()}, bullet_pos);
}

PhysicsState PhysicsEngine::getPhysicsStateImpact() const {
    return PhysicsState({tanks[0].getOrientation(), tanks[1].getOrientation()}, impact_pos);
}

TankOrientations PhysicsEngine::getTankOrientations() const {
    return TankOrientations({tanks[0].getOrientation(), tanks[1].getOrientation()});
}

void PhysicsEngine::explodeBullet() {
    for(int i : {0, 1}) {
        tanks[i].applyForce(bullet_pos);
    }

    bullet_pos = {-1, -1};
}

// If this function changes, please make sure that events are still returned in
// the following order (not all events required every time). This ensures that
// things are displayed correctly in the frontend.
//
// For respawn actions:
// * RespawnEvent
// * Movemet
// * FallOffMap
// For shoot actions:
// * ShootEvent
// * Movement for bullet
// * Bullet impact
// * Movement for tanks
// * FallOffMap
std::vector<History::HistoryObject>
PhysicsEngine::handleAction(TankOrientations tank_orientations,
                             std::uint8_t player_id,
                             bool simulated,
                             boost::variant<ShootCommit, RespawnCommit> const& action) {
    std::vector<History::HistoryObject> history;

    // We might want to ignore motions for queries because they can take up a lot
    // of storage for some information that we're gonna ignore later anyway.
    // NOTE / TODO: disabling logging of query motions breaks queries for websocket
    // players in the browser version of TankGolf, so set this to true if you need them.
    const bool log_query_motions = true;

    std::array<bool, 2> player_on_map_at_beginning_of_turn;
    for (std::size_t i = 0; i < 2; ++i) {
        player_on_map_at_beginning_of_turn[i] = tank_orientations[i].pos != point{-1, -1};
        tanks[i].stopMoving();
    }

    apply_lambda_visitor<void>(
        action,
        [&](ShootCommit const& command){
            setFromState(tank_orientations);
            auto shoot_result = shoot(player_id, command);
            history.emplace_back(History::ShootEvent{command.angle, command.intensity},
                                 player_id, simulated);

            History::Motion bullet_flight = Util::map<point, PhysicsState>(
                                                [&](point const& p){
                                                    return PhysicsState(tank_orientations, p);
                                                }, shoot_result.first);
            if (log_query_motions || !simulated) {
                history.emplace_back(bullet_flight, player_id, simulated);
            }

            if (shoot_result.second) {
                const point impact_pos = shoot_result.first.back();
                // bullet hit something, simulate impact
                history.emplace_back(History::BulletImpactEvent{impact_pos},
                                     player_id, simulated);

                explodeBullet();

                History::Motion tank_movement = Util::map<TankOrientations, PhysicsState>(
                                                    [&](auto const& tos) {
                                                        return PhysicsState(tos);
                                                    }, moveTanks());
                if (log_query_motions || !simulated) {
                    history.emplace_back(tank_movement, player_id, simulated);
                }

            } else {
                // bullet left map, do nothing
            }
        },
        [&](RespawnCommit const& command){
            history.emplace_back(History::RespawnEvent{command.x_position},
                                 player_id, simulated);
            player_on_map_at_beginning_of_turn[player_id] = true;

            const float eps = 0.001;
            if (command.x_position < map.respawn_x_limits[0] - eps ||
                command.x_position > map.respawn_x_limits[1] + eps) {

                // The tank will be detected as "fell off map" because it was set to be on
                // the map at the beginning of the turn, and Tank::deactivate() removes it
                // from the map. So the log will contain a respawn event and a fall off map
                // event.
                tanks[player_id].deactivate();

#ifdef TANKGOLF_VERBOSE
                std::cerr << "Respawn position " << command.x_position
                          << " is outside the allowed range.\n";
#endif

            } else {
                tank_orientations[player_id].pos = {command.x_position, map.respawn_height};
                setFromState(tank_orientations);

                History::Motion tank_movement = Util::map<TankOrientations, PhysicsState>(
                                                        [&](auto const& tos) {
                                                            return PhysicsState(tos);
                                                        }, moveTanks());
                if (log_query_motions || !simulated) {
                    history.emplace_back(tank_movement, player_id, simulated);
                }
            }
        }
    );

    tank_orientations = getTankOrientations();

    for (std::uint8_t i = 0; i < 2; ++i) {
        if (player_on_map_at_beginning_of_turn[i] && tank_orientations[i].pos == point{-1, -1}) {
            history.emplace_back(History::FallOffMapEvent{i},
                                 player_id, simulated);
        }
    }

    return history;
}
