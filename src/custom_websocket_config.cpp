#include "custom_websocket_config.hpp"

ConnectionData::ConnectionData()
    : game_id(-1), received_messages (std::make_shared<blocking_queue<std::string>>())
{}
