#include <iostream>
#include <string>
#include <thread>

#include "game.hpp"

int main(int argc, char* argv[]) {
    if (argc < 4 || argc > 5) {
        std::cerr << "Usage: ./tankgolf_cli [endless|<target score>] botA_command botB_command [logfile]\n";
        std::cerr << "       The logfile path is optional. If no logfile\n";
        std::cerr << "       path is given, the log won't be recorded.\n";

        for (int i = 1; i < argc; ++i) {
            std::cerr << argv[i] << '\n';
        }
        return 1;
    }

    std::string target_str = argv[1];

    std::string botA_command = argv[2], botB_command = argv[3];
    std::array<PlayerType, 2> players = {
        PlayerTypes::Bot{botA_command},
        PlayerTypes::Bot{botB_command}
    };

    std::unique_ptr<Logger> logger;
    if (argc == 5) {
        std::string logfile = argv[4];
        // TODO: add time to log name
        logger = std::make_unique<FileLogger>(logfile);
    } else {
        // Creates an empty combined logger
        logger = std::make_unique<CombinedLogger>(std::vector<std::unique_ptr<Logger>>());
    }

    // Using a relative path starting from project root here, because that's where
    // the binary will be called from in the `run_match.sh` script.
    Map map("geometry/maps/map1.json");

    const unsigned target_score = (target_str == "endless") ?
                                std::numeric_limits<unsigned>::max() :
                                std::stoi(target_str);

    HeadlessGameHost host(players, map, std::move(logger), target_score);
    host.join();
}
