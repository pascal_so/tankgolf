// This bot is meant to get you started if you know how C++ structs work and
// you don't want to spend time with the parsing of objects.

// Some useful stuff is provided in this header file. Have a look at the main
// function below first to see how they are used, then maybe check out the code
// in "helper_structs_and_functions.hpp" later.
#include "helper_structs_and_functions.hpp"


// ######## Actual bot code ###########
// The bot just shoots in the same direction every time, so it's not a very
// good bot, it's just here to show you how to use the provided helper
// functions.

int main() {
    bool running = true;

    while (running) {
        RoundType round_type;
        GameState game_state;

        std::tie(round_type, game_state) = startRound();

        switch (round_type) {
        case RoundType::Respawn: {
            float spawn_position = 3;

            // Check the current opponent position (opponent is always at tanks[1])
            if (game_state.tanks[1].x == -1) {
                // Position of -1 means, that the opponent is currently not on
                // the map, so the opponent probably fell off the map or we are
                // spawning first and this is the beginning of the game.
            } else {
                // spawn next to the opponent
                spawn_position = game_state.tanks[1].x + 1;
            }

            GameState preview_state = queryRespawn(spawn_position);

            if (preview_state.scores[1] > game_state.scores[1]) {
                // score of opponent would increase, this means we fall off
                // the map if we spawn at this location, so we change the
                // location.

                spawn_position = 5;
            }

            commitRespawn(spawn_position);

            break;
        } case RoundType::Shoot: {
            // Shoot with the same angle and intensity every time.

            commitShoot(0.3, 0.9);

            break;
        } case RoundType::GameOver: {
            running = false;
            break;
        }}
    }
}
