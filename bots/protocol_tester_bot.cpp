// Protocol tester bot

// This bot doesn't do anything useful strategy wise, it only makes sure to use
// all of the functionality provided by the protocol and throws if something
// does not seem right.

#include <iostream>
#include <array>
#include <string>

#ifndef M_PI
#define M_PI 3.14159265358979
#endif

#include "helper_structs_and_functions.hpp"

const float epsillon = 0.01;

void check(float x, float y) {
    assert((x >= -epsillon && y >= -epsillon) || 
           (x == -1        && y == -1));
}

void check(Orientation const& orientation) {
    assert(-epsillon < orientation.rot && orientation.rot < M_PI * 2 + epsillon);
    check(orientation.x, orientation.y);
}

void check(GameState const& state) {
    for (auto const& t : state.tanks)
        check(t);
    check(state.last_impact_x, state.last_impact_y);

    for (auto s : state.scores) {
        assert(s >= 0);
    }
}

int main() {
    GameState state;
    RoundType type;

    while (true) {
        std::tie(type, state) = startRound();
        if (type == RoundType::GameOver)
            break;

        check(state);

        switch (type) {
            case RoundType::Shoot: {
                check(queryShoot(-M_PI / 2, 1));
                check(fullQueryShoot(state.tanks, M_PI / 2, 0));

                commitShoot(0, 0.5);
            } break;
            case RoundType::Respawn: {
                check(queryRespawn(2));
                check(fullQueryRespawn(state.tanks, 2));

                const std::array<Orientation, 2> tanks = {
                    Orientation{-1, -1, 0},
                    Orientation{-1, -1, 0}
                };
                auto state = fullQueryRespawn(tanks, 2);
                check(state);
                assert(state.tanks[1].x == -1 && state.tanks[1].y == -1);

                commitRespawn(4);
            } break;
            case RoundType::GameOver: {
            } break;
        }
    }

    std::string dummy;
    std::cin >> dummy;
    assert(!std::cin);
}
