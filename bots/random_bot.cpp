#include <iostream>
#include <string>
#include <random>

#ifndef M_PI
#define M_PI 3.14159265358979
#endif

const float min_x_pos = 2.f;
const float max_x_pos = 15.5f;

void ignoreGameState() {
    float devnull;
    std::cin >> devnull >> devnull >> devnull; // Tank A
    std::cin >> devnull >> devnull >> devnull; // Tank B
    std::cin >> devnull >> devnull; // Bullet Impact pos
    std::cin >> devnull >> devnull; // scores
}

int main() {
    std::mt19937 rng (0);

    std::uniform_real_distribution<float> angle_dist (-M_PI/2 + 0.05, M_PI/2 - 0.05);
    std::uniform_real_distribution<float> intensity_dist (0.1, 1);
    std::uniform_real_distribution<float> x_pos_dist (min_x_pos, max_x_pos);

    std::string command;
    while (std::cin >> command) {
        ignoreGameState();

        if (command == "startshoot") {
            if (rng() & 1) {
                std::cout << "queryshoot " << angle_dist(rng) << ' ' << intensity_dist(rng) << std::endl;
                std::string devnull; std::cin >> devnull;
                ignoreGameState();
            }

            std::cout << "shoot " << angle_dist(rng) << ' ' << intensity_dist(rng) << std::endl;
        } else if (command == "startrespawn") {
            if (rng() & 1) {
                std::cout << "queryrespawn " << x_pos_dist(rng) << std::endl;
                std::string devnull; std::cin >> devnull;
                ignoreGameState();
            }

            std::cout << "respawn " << x_pos_dist(rng) << std::endl;
        } else if (command == "gameover") {
            break;
        }
    }

    std::cerr << "Random Bot: exiting.\n";
}
