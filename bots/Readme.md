# Bots

Place your cpp bots in here with the name `*_bot.cpp`.

When compiling the entire project with cmake, the compiled bot will end up in `build/bots/`.

If you need to include any of the files from the tankgolf engine, you'll have to modify the
`CMakeLists.txt` yourself, as currently this won't compile.
